#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <list>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

int camMode = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

//add ability to multiply a vector by a matrix
Vector operator*(const Matrix& m, const Vector& v) {
    return Vector(
            m.values[0] * v.x() + m.values[4] * v.y() + m.values[8] * v.z() + m.values[12] * v.w(),
            m.values[1] * v.x() + m.values[5] * v.y() + m.values[9] * v.z() + m.values[13] * v.w(),
            m.values[2] * v.x() + m.values[6] * v.y() + m.values[10] * v.z() + m.values[14] * v.w());
}

//add ability to compare Vectors
bool operator==(const Vector &a, const Vector &b)
{
    return a.x() == b.x() && a.y() == b.y() && a.z() == b.z();
}

//add ability to add Vectors
Vector operator+(const Vector& u, const Vector& v) {
    return Vector(
            u.x() + v.x(),
            u.y() + v.y(),
            u.z() + v.z(),
            u.w() + v.w());
}

Matrix processModel(const Matrix& model, Camera * cam1, Camera * cam2, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_LEFT_BRACKET)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_RIGHT_BRACKET)) { trans.rotate_y(ROT); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP) && camMode != 2) {
        trans.translate(0, 0, -TRANS);
        cam1->eye = cam1->eye - Vector(0,0,TRANS);
        cam1->origin = cam1->origin - Vector(0,0,TRANS);
        cam2->eye = cam2->eye - Vector(0,0,TRANS);
        cam2->origin = cam2->origin - Vector(0,0,TRANS);}
    else if (isPressed(window, GLFW_KEY_DOWN) && camMode != 2) {
        trans.translate(0, 0, TRANS);
        cam1->eye = cam1->eye - Vector(0,0,-TRANS);
        cam1->origin = cam1->origin - Vector(0,0,-TRANS);
        cam2->eye = cam2->eye - Vector(0,0,-TRANS);
        cam2->origin = cam2->origin - Vector(0,0,-TRANS);}
    else if (isPressed(window, GLFW_KEY_LEFT) && camMode != 2) {
        trans.translate(-TRANS, 0, 0);
        cam1->eye = cam1->eye - Vector(TRANS, 0, 0);
        cam1->origin = cam1->origin - Vector(TRANS, 0, 0);
        cam2->eye = cam2->eye - Vector(TRANS, 0, 0);
        cam2->origin = cam2->origin - Vector(TRANS, 0, 0);}
    else if (isPressed(window, GLFW_KEY_RIGHT) && camMode != 2) {
        trans.translate(TRANS, 0, 0);
        cam1->eye = cam1->eye - Vector(-TRANS, 0, 0);
        cam1->origin = cam1->origin - Vector(-TRANS, 0, 0);
        cam2->eye = cam2->eye - Vector(-TRANS, 0, 0);
        cam2->origin = cam2->origin - Vector(-TRANS, 0, 0);}
    //Translations for camera mode 2
    else if ((isPressed(window, GLFW_KEY_UP) && camMode == 2)) {
        Vector gaze = cam2->origin-cam2->eye;
        Vector w = gaze.scale(TRANS).normalized().scale(2);
        trans.translate(w.x(), 0, w.z());
        cam1->eye = cam1->eye - Vector(-w.x(), 0, -w.z());
        cam1->origin = cam1->origin - Vector(-w.x(), 0, -w.z());
        cam2->eye = cam2->eye - Vector(-w.x(), 0, -w.z());
        cam2->origin = cam2->origin - Vector(-w.x(), 0, -w.z());
    } else if ((isPressed(window, GLFW_KEY_DOWN) && camMode == 2)) {
        Vector gaze = cam2->origin-cam2->eye;
        Vector w = gaze.scale(TRANS).normalized().scale(2);
        trans.translate(-w.x(), 0, -w.z());
        cam1->eye = cam1->eye - Vector(w.x(), 0, w.z());
        cam1->origin = cam1->origin - Vector(w.x(), 0, w.z());
        cam2->eye = cam2->eye - Vector(w.x(), 0, w.z());
        cam2->origin = cam2->origin - Vector(w.x(), 0, w.z());
    } else if ((isPressed(window, GLFW_KEY_LEFT) && camMode == 2)) {
        Matrix rot;
        rot.rotate_y(-ROT);
        cam2->eye = rot * cam2->eye;
    } else if ((isPressed(window, GLFW_KEY_RIGHT) && camMode == 2)) {
        Matrix rot;
        rot.rotate_y(ROT);
        cam2->eye = rot * cam2->eye;
    }
    //Cam Mode
    else if (isPressed(window, GLFW_KEY_1)) {camMode = 0;}
    else if (isPressed(window, GLFW_KEY_2)) {camMode = 1;}
    else if (isPressed(window, GLFW_KEY_3)) {camMode = 2;}
    //Camera Transformations
    else if (isPressed(window, GLFW_KEY_S) && camMode==2) {
        cam2->eye = cam2->eye - Vector(0,.001,0);
        cam2->origin = cam2->origin - Vector(0,-.001,0);}
    else if (isPressed(window, GLFW_KEY_W) && camMode==2) {
        cam2->eye = cam2->eye - Vector(0,-.001,0);
        cam2->origin = cam2->origin - Vector(0,.001,0);}

    return trans * model;
}

void processInput(Matrix& model, Camera * cam1, Camera * cam2, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, cam1, cam2, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

struct Face {
    Vector a = Vector(0,0,0);
    Vector b = Vector(0,0,0);
    Vector c = Vector(0,0,0);
    Vector color = Vector(0,0,0);
    Vector aN = Vector(0,0,0);
    Vector bN = Vector(0,0,0);
    Vector cN = Vector(0,0,0);
    Vector normal = Vector(0,0,0);
};

std::vector<float> readInOBJ (FILE * file, Vector color) {
    std::vector< unsigned int > vertexIndices;
    std::vector< Vector > temp_vertices;
    std::list< Face > faces;
    int res;
    //read in obj file
    do {
        char lineHeader[128];
        res = fscanf(file, "%s", lineHeader);

        //Read in vertices;
        if ( strcmp( lineHeader, "v" ) == 0 ) {
            float x, y, z;
            fscanf(file, "%f %f %f\n", &x, &y, &z);
            Vector vertex(x,y,z);
            //std::cout << vertex << std::endl;
            temp_vertices.push_back(vertex);

        }else if ( strcmp( lineHeader, "f" ) == 0 ) {
            Face temp;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0],
                                                         &vertexIndex[1], &uvIndex[1], &normalIndex[1],
                                                         &vertexIndex[2], &uvIndex[2], &normalIndex[2] );

            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
        }
    } while(res != EOF);

    //convert obj file to format for opengl
    std::vector<float> coords;
    for (int i = 0; i < vertexIndices.size(); i++) {
        Face temp;
        temp.a = temp_vertices[vertexIndices[i++]-1];
        temp.b = temp_vertices[vertexIndices[i++]-1];
        temp.c = temp_vertices[vertexIndices[i]-1];

        temp.color = color;

        Vector lineAB = temp.b - temp.a;
        Vector lineAC = temp.c - temp.a;

        Vector normal = lineAB.cross(lineAC);
        normal = normal.normalized();

        temp.normal = normal;

        coords.push_back(temp.a.x());
        coords.push_back(temp.a.y());
        coords.push_back(temp.a.z());

        coords.push_back(temp.color.x());
        coords.push_back(temp.color.y());
        coords.push_back(temp.color.z());

        coords.push_back(temp.normal.x());
        coords.push_back(temp.normal.y());
        coords.push_back(temp.normal.z());

        coords.push_back(temp.b.x());
        coords.push_back(temp.b.y());
        coords.push_back(temp.b.z());

        coords.push_back(temp.color.x());
        coords.push_back(temp.color.y());
        coords.push_back(temp.color.z());

        coords.push_back(temp.normal.x());
        coords.push_back(temp.normal.y());
        coords.push_back(temp.normal.z());

        coords.push_back(temp.c.x());
        coords.push_back(temp.c.y());
        coords.push_back(temp.c.z());

        coords.push_back(temp.color.x());
        coords.push_back(temp.color.y());
        coords.push_back(temp.color.z());

        coords.push_back(temp.normal.x());
        coords.push_back(temp.normal.y());
        coords.push_back(temp.normal.z());
    }

    return coords;
}

int main() {
    GLFWwindow *window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-program2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    //create maze
    FILE * mazeFile = fopen("../models/maze.obj", "r");
    Vector colorM(0.5,0.5,0.5);
    Model maze(
            readInOBJ(mazeFile, colorM),
            Shader("../vert.glsl", "../frag.glsl"));
    //create avatar
    FILE * avatarFile = fopen("../models/warship.obj", "r");
    Vector colorA(0.722,0.525,0.043);
    Model avatar(
            readInOBJ(avatarFile, colorA),
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix avaScale, avaTrans, avaRot;
    avaScale.scale(0.05, 0.05, 0.05);
    avaTrans.translate(-11, 0.2, -9.6);
    avaRot.rotate_y(-90);
    avatar.model = avaTrans*avaScale*avaRot;

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, -50);

    Camera birdsEye;
    birdsEye.projection = projection;
    birdsEye.eye = Vector(-11, 2, -9.6);
    birdsEye.origin = Vector(-11, 0, -9.6);
    birdsEye.up = Vector(0, 0, -1);

    Camera birdsCenter;
    birdsCenter.projection = projection;
    birdsCenter.eye = Vector(0, 10, 0);
    birdsCenter.origin = Vector(0, 0, 0);
    birdsCenter.up = Vector(0, 0, -1);

    Camera third;
    third.projection = projection;
    third.eye = Vector(-11.5, 0.8, -9.6);
    third.origin = Vector(-11, 0.4, -9.6);
    third.up = Vector(0, 1, 0);


    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(5.0f, 10.0f, 3.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(avatar.model, &birdsEye, &third, window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Determine Viewpoint
        Camera cam;
        if(camMode == 0) {
            cam = birdsCenter;
        } else if(camMode == 1) {
            cam = birdsEye;
        } else {
            cam = third;
        }

        // render the object and the floor
        renderer.render(cam, avatar, lightPos);
        renderer.render(cam, maze, lightPos);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}