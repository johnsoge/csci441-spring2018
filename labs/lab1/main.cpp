#include <iostream>

#include "bitmap_image.hpp"

using namespace std;

int main(int argc, char** argv) {

    float x1, x2, x3, y1, y2, y3, xmin, xmax, ymin, ymax, r1, g1, b1, r2, g2, b2, r3, g3, b3;

    //c.ignore found from https://stackoverflow.com/questions/7945082/insert-multiple-inputs-on-one-line-in-c?rq=1
    cout << "Enter 3 x and y coordinates: " << endl;
    cin >> x1;
    cin.ignore(1, ',');
    cin >> y1;
    cin.ignore(1, ':');
    cin >> r1;
    cin.ignore(1, ',');
    cin >> g1;
    cin.ignore(1, ',');
    cin >> b1;

    xmin = x1; xmax = x1;
    ymin = y1; ymax = y1;

    cin >> x2;
    cin.ignore(1, ',');
    cin >> y2;
    cin.ignore(1, ':');
    cin >> r2;
    cin.ignore(1, ',');
    cin >> g2;
    cin.ignore(1, ',');
    cin >> b2;

    if (x2 < xmin)
        xmin = x2;
    else if (x2 > xmax)
        xmax = x2;
    if (y2 < ymin)
        ymin = y2;
    else if (y2 > ymax)
        ymax = y2;

    cin >> x3;
    cin.ignore(1, ',');
    cin >> y3;
    cin.ignore(1, ':');
    cin >> r3;
    cin.ignore(1, ',');
    cin >> g3;
    cin.ignore(1, ',');
    cin >> b3;

    if (x3 < xmin)
        xmin = x3;
    else if (x3 > xmax)
        xmax = x3;
    if (y3 < ymin)
        ymin = y3;
    else if (y3 > ymax)
        ymax = y3;

    cout << "You entered: " << endl
         << x1 << ", " << y1 << endl
         << x2 << ", " << y2 << endl
         << x3 << ", " << y3 << endl;

    /*
      Prompt user for 3 points separated by whitespace.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    float fac = (y1 - y3)*x2 + (x3 - x1)*y2 + x1*y3 - x3*y1;
    float fab = (y1 - y2)*x3 + (x2 - x1)*y3 + x1*y2 - x2*y1;

    for (float x = xmin; x <= xmax; x++)
        for (float y = ymin; y <= ymax; y++)
        {
            float beta = ((y1 - y3)*x + (x3 - x1)*y + x1*y3 - x3*y1)/fac;
            float gamma = ((y1 - y2)*x + (x2 - x1)*y + x1*y2 - x2*y1)/fab;
            float alpha = 1 - beta - gamma;

            int red = (alpha * r1 + beta * r2 + gamma * r3)*255;
            int green = (alpha * g1 + beta * g2 + gamma * g3)*255;
            int blue = (alpha * b1 + beta* b2 + gamma * b3)*255;

            if (alpha < 1 && alpha > 0 && beta > 0 && beta < 1 && gamma > 0 && gamma < 1) {
                rgb_t color = make_colour(red, green, blue);
                image.set_pixel(x, y, color);
            }
        }
    /*

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is, color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    image.save_image("triangle_color.jpg");
    std::cout << "Success" << std::endl;
}
