#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <list>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"

const int SCREEN_WIDTH = 1000;
const int SCREEN_HEIGHT = 1000;
float camX = 0, camY = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

Circle temp(20, 0.5);
int numShapes = 1;

struct squareStruct {
    GLuint VAO = 0;
    Square s;
    Matrix model;
    int id;
    float r, g, b;
    float rot, xS, yS;
};
struct triangleStruct {
    GLuint VAO = 0;
    Triangle s;
    Matrix model;
    int id;
    float r, g, b;
    float rot, xS, yS;
};
struct circleStruct {
    GLuint VAO = 0;
    Circle s = temp;
    Matrix model;
    int id;
    float r, g, b;
    float rot, xS, yS;
};
struct customStruct {
    GLuint VAO = 0;
    std::list< Vector > vertices;
    std::vector<float> coords;
    Matrix model;
    int id;
    float r, g, b;
    float rot, xS, yS;
};

//add ability to compare structs
bool operator==(const squareStruct &a, const squareStruct &b)
{
    return a.id == b.id;
}
bool operator==(const triangleStruct &a, const triangleStruct &b)
{
    return a.id == b.id;
}
bool operator==(const circleStruct &a, const circleStruct &b)
{
    return a.id == b.id;
}
bool operator==(const customStruct &a, const customStruct &b)
{
    return a.id == b.id;
}

std::list <squareStruct> squareList = {};
std::list <triangleStruct> triangleList = {};
std::list <circleStruct> circleList = {};
std::list <customStruct> customList = {};

//Functions that add a shape to a list
void addSquare (Square shape) {
    squareStruct curr;

    // copy vertex data for triangle
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, shape.coords.size()*sizeof(float), &shape.coords[0], GL_DYNAMIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //add information to the struct for the shape
    curr.s = shape;
    curr.VAO = VAO;
    curr.id = numShapes;
    numShapes++;

    float xT, yT, xS, yS, r, rC, gC, bC;
    //Prompt User for input
    std::cout << "Enter a point to center shape at, with (0,0) as the center of the window: X,Y " << std::endl;
    std::cin >> xT;
    std::cin.ignore(1, ',');
    std::cin >> yT;

    Matrix trans;
    trans.translate(xT + camX, yT + camY, 0);

    std::cout << "Enter a scale for the X and Y dimension of the shape: X-scale, Y-scale " << std::endl;
    std::cin >> xS;
    std::cin.ignore(1, ',');
    std::cin >> yS;

    Matrix scale;
    curr.xS = xS;
    curr.yS = yS;
    scale.scale(xS, yS, 1.0);

    std::cout << "Enter a rotation for the shape in degrees: " << std::endl;
    std::cin >> r;

    Matrix rotate;
    curr.rot = r;
    rotate.rotate_z(r);

    curr.model = trans * scale * rotate * curr.model;


    std::cout << "Enter 3 color values between 0 and 1: R,G,B" << std::endl;
    std::cin >> rC;
    std::cin.ignore(1, ',');
    std::cin >> gC;
    std::cin.ignore(1, ',');
    std::cin >> bC;

    curr.r = rC;
    curr.g = gC;
    curr.b = bC;

    std::cout << "Square ID: " << curr.id << std::endl;

    squareList.push_back(curr);
}

void addTriangle (Triangle shape) {
    triangleStruct curr;

    // copy vertex data for triangle
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, shape.coords.size()*sizeof(float), &shape.coords[0], GL_DYNAMIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //add information to the struct for the shape
    curr.s = shape;
    curr.VAO = VAO;
    curr.id = numShapes;
    numShapes++;

    float xT, yT, xS, yS, r, rC, gC, bC;;
    //Prompt User for input
    std::cout << "Enter a point to center shape at, with (0,0) as the center of the window: X,Y " << std::endl;
    std::cin >> xT;
    std::cin.ignore(1, ',');
    std::cin >> yT;

    Matrix trans;
    trans.translate(xT + camX, yT + camY, 0);

    std::cout << "Enter a scale for the X and Y dimension of the shape: X-scale, Y-scale " << std::endl;
    std::cin >> xS;
    std::cin.ignore(1, ',');
    std::cin >> yS;

    Matrix scale;
    curr.xS = xS;
    curr.yS = yS;
    scale.scale(xS, yS, 1.0);

    std::cout << "Enter a rotation for the shape in degrees: " << std::endl;
    std::cin >> r;

    Matrix rotate;
    curr.rot = r;
    rotate.rotate_z(r);

    curr.model = trans * scale * rotate * curr.model;

    std::cout << "Enter 3 color values between 0 and 1: R,G,B" << std::endl;
    std::cin >> rC;
    std::cin.ignore(1, ',');
    std::cin >> gC;
    std::cin.ignore(1, ',');
    std::cin >> bC;

    curr.r = rC;
    curr.g = gC;
    curr.b = bC;

    std::cout << "Shape ID: " << curr.id << std::endl;

    triangleList.push_back(curr);
}

void addCircle (Circle shape) {
    circleStruct curr;

    // copy vertex data for triangle
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, shape.coords.size()*sizeof(float), &shape.coords[0], GL_DYNAMIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //add information to the struct for the shape
    curr.s = shape;
    curr.VAO = VAO;
    curr.id = numShapes;
    numShapes++;

    float xT, yT, xS, yS, r, rC, gC, bC;;
    //Prompt User for input
    std::cout << "Enter a point to center shape at, with (0,0) as the center of the window: X,Y " << std::endl;
    std::cin >> xT;
    std::cin.ignore(1, ',');
    std::cin >> yT;

    Matrix trans;
    trans.translate(xT + camX, yT + camY, 0);

    std::cout << "Enter a scale for the X and Y dimension of the shape: X-scale, Y-scale " << std::endl;
    std::cin >> xS;
    std::cin.ignore(1, ',');
    std::cin >> yS;

    Matrix scale;
    curr.xS = xS;
    curr.yS = yS;
    scale.scale(xS, yS, 1.0);

    std::cout << "Enter a rotation for the shape in degrees: " << std::endl;
    std::cin >> r;

    Matrix rotate;
    curr.rot = r;
    rotate.rotate_z(r);

    curr.model = trans * scale * rotate * curr.model;

    std::cout << "Enter 3 color values between 0 and 1: R,G,B" << std::endl;
    std::cin >> rC;
    std::cin.ignore(1, ',');
    std::cin >> gC;
    std::cin.ignore(1, ',');
    std::cin >> bC;

    curr.r = rC;
    curr.g = gC;
    curr.b = bC;

    std::cout << "Shape ID: " << curr.id << std::endl;

    circleList.push_back(curr);
}

bool operator == (const Vector &a, const Vector &b) {
    return a.x() == b.x() &&
           a.y() == b.y();
}

void findEars (customStruct * curr) {
    std::cout << "Finding ears." << std::endl;
    std::list<Vector>::const_iterator vert = curr->vertices.begin(), end = curr->vertices.end();
    while (vert != end) {
        //get the 3 current vertices to look at
        Vector p1 = *vert++;
        std::list<Vector>::const_iterator pt = vert;
        Vector p2 = *vert++; Vector p3 = *vert;
        //reset iterator back to the first vertex
        vert--; vert--;

        //calculate vectors
        Vector a (p1.x() - p1.x(), p1.y() - p2.y(), 0);
        //std::cout << "Vector a: " << a << std::endl;
        Vector b (p3.x() - p2.x(), p3.y() - p2.y(), 0);
        //std::cout << "Vector b: " << b << std::endl;

        double angle = acos(a.x()*b.x() + a.y()*b.y() /
                       sqrt(a.x()*a.x() + a.y()*a.y())*sqrt(b.x()*b.x() + b.y()*b.y()));
        //std::cout << "Angle: " << angle << std::endl;

        if (angle <= M_PI && curr->vertices.size() >= 3) {
            std::cout << "Found an ear." << std::endl;
            //Ear found
                //Add first point of triangle
            curr->coords.push_back(p1.x());
            curr->coords.push_back(p1.y());
                //Add second point of triangle
            curr->coords.push_back(p2.x());
            curr->coords.push_back(p2.y());
                //Add third point of triangle
            curr->coords.push_back(p3.x());
            curr->coords.push_back(p3.y());

            curr->vertices.erase(pt);
        } else {
            //go to next vertice
            //std::cout << "Did not find an ear." << std::endl;
            vert++;
        }

        end = curr->vertices.end();
    }
}

void addCustom () {
    customStruct curr;
    int num = 0;

    std::cout << "Enter the number of vertices for the shape: " << std::endl;
    std::cin >> num;

    for(int i = 0; i < num; i++) {
        float x, y;
        std::cout << "Enter a vertex, in counter-clockwise order: X,Y" << std::endl;
        std::cin >> x;
        std::cin.ignore(1, ',');
        std::cin >> y;

        Vector temp(x, y, 0);
        curr.vertices.push_back(temp);
    }

    findEars(&curr);

    //std::cout << "Coords size: " << curr.coords.size() << std::endl;

    // copy vertex data for triangle
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, curr.coords.size()*sizeof(float), &curr.coords[0], GL_DYNAMIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //add information to the struct for the shape
    curr.VAO = VAO;
    curr.id = numShapes;
    numShapes++;

    float xT, yT, xS, yS, r, rC, gC, bC;;
    //Prompt User for input
    std::cout << "Enter a point to center shape at, with (0,0) as the center of the window: X,Y " << std::endl;
    std::cin >> xT;
    std::cin.ignore(1, ',');
    std::cin >> yT;

    Matrix trans;
    trans.translate(xT + camX, yT + camY, 0);

    std::cout << "Enter a scale for the X and Y dimension of the shape: X-scale, Y-scale " << std::endl;
    std::cin >> xS;
    std::cin.ignore(1, ',');
    std::cin >> yS;

    Matrix scale;
    curr.xS = xS;
    curr.yS = yS;
    scale.scale(xS, yS, 1.0);

    std::cout << "Enter a rotation for the shape in degrees: " << std::endl;
    std::cin >> r;

    Matrix rotate;
    curr.rot = r;
    rotate.rotate_z(r);

    curr.model = trans * scale * rotate * curr.model;

    std::cout << "Enter 3 color values between 0 and 1: R,G,B" << std::endl;
    std::cin >> rC;
    std::cin.ignore(1, ',');
    std::cin >> gC;
    std::cin.ignore(1, ',');
    std::cin >> bC;

    curr.r = rC;
    curr.g = gC;
    curr.b = bC;

    std::cout << "Shape ID: " << curr.id << std::endl;

    customList.push_back(curr);

}

float zoom = 1.0;

Vector windowPress(0,0,0), windowCurrent(0,0,0);

void convertAndSendCameraCoords () {
    //convert mouse press and release locations into world coordinates
    float xPress = (-1.0f + windowPress.x() * (2.0f / SCREEN_WIDTH));
    float yPress = (-1.0f + windowPress.y() * (2.0f / SCREEN_HEIGHT));
    float xCurrent = (-1.0f + windowCurrent.x() * (2.0f / SCREEN_WIDTH));
    float yCurrent = (-1.0f + windowCurrent.y() * (2.0f / SCREEN_HEIGHT));

    camX = camX + (xPress - xCurrent);
    camY = camY - (yPress - yCurrent);
}

bool view = false;

int edit = 0;
const int NONE = 0;
const int STAMP = 1;
const int CUSTOM = 2;
const int GROUP = 3;
const int MOD = 4;
//look for different key releases
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    //enter View and Edit mode
    if (key == GLFW_KEY_V && action == GLFW_RELEASE) {
        view = true;
        std::cout << "Entered View mode." << std::endl;
    }

    if (key == GLFW_KEY_E && action == GLFW_RELEASE) {
        view = false;
        edit = NONE;
        std::cout << "Entered Edit mode." << std::endl;
    }

    if(view) {
        //Look to shrink viewport
        if (key == GLFW_KEY_MINUS && action == GLFW_RELEASE) {
            zoom = zoom + 0.1f;
            std::cout << "Zoomed out." << std::endl;
        }
        //look to increase viewport
        if (key == GLFW_KEY_EQUAL && action == GLFW_RELEASE) {
            zoom = zoom - 0.1f;
            std::cout << "Zoomed in." << std::endl;
        }
    }

    if(!view) {
        //Look for input to determine edit mode
        if (key == GLFW_KEY_1 && action == GLFW_RELEASE) {
            edit = STAMP;
            std::cout << "Entered Stamp sub-mode." << std::endl;
        }
        if (key == GLFW_KEY_2 && action == GLFW_RELEASE) {
            edit = CUSTOM;
            std::cout << "Entered Custom sub-mode." << std::endl;
        }
        if (key == GLFW_KEY_3 && action == GLFW_RELEASE) {
            edit = GROUP;
            std::cout << "Entered Group sub-mode." << std::endl;
        }
        if (key == GLFW_KEY_4 && action == GLFW_RELEASE) {
            edit = MOD;
            std::cout << "Entered Modify sub-mode." << std::endl;
        }
    }

    if(edit == STAMP) {
        //look to create new square
        if (key == GLFW_KEY_S && action == GLFW_RELEASE) {
            Square s;
            addSquare(s);
        }
        //look to create new triangle
        if (key == GLFW_KEY_T && action == GLFW_RELEASE) {
            Triangle t;
            addTriangle(t);
        }
        //look to create new circle
        if (key == GLFW_KEY_C && action == GLFW_RELEASE) {
            Circle c(20, 0.5);
            addCircle(c);
        }
    } else if(edit == CUSTOM) {
        if (key == GLFW_KEY_C && action == GLFW_RELEASE) {
            std::cout << "Creating Custom shape." << std::endl;
            addCustom();
        }
    } else if(edit == GROUP) {
        //TODO add ability to group shapes

    }

}

bool mousePressed = false;
bool mouseReleased = false;
//Look for mouse input
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && view){
        mousePressed = true;
        double xpos, ypos;

        glfwGetCursorPos(window, &xpos, &ypos);

        Vector press(xpos, ypos, 0);
        windowPress = press;
    }
    //look for the mouse to be released
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        mouseReleased = true;
        mousePressed = false;
        if(view) {

        }
    }
}

//Helper Functions to make the draw loop less complicated
void drawSquares (Shader shader) {
    //Draw all squares
    for( squareStruct shape : squareList) {
        glBindVertexArray(shape.VAO);
        //set the transformation for this shape
        Uniform::set(shader.id(), "model", shape.model);

        if(mouseReleased && edit == MOD) {
            // Convert id into an RGB color
            int r = (shape.id & 0x000000FF) >>  0;
            int g = (shape.id & 0x0000FF00) >>  8;
            int b = (shape.id & 0x00FF0000) >> 16;

            int locationPC = glGetUniformLocation(shader.id(),  "pickingColor");
            glUniform3f(locationPC, r/255.0f, g/255.0f, b/255.0f);
        } else {
            int locationC = glGetUniformLocation(shader.id(),  "ourColor");
            glUniform3f(locationC, shape.r, shape.g, shape.b);
        }

        glDrawArrays(GL_TRIANGLES, 0, shape.s.coords.size() * sizeof(float));
    }
}

void drawTriangles (Shader shader) {
    //Draw all triangles
    for( triangleStruct shape : triangleList) {
        glBindVertexArray(shape.VAO);
        //set the transformation for this shape
        Uniform::set(shader.id(), "model", shape.model);

        if(mouseReleased && edit == MOD) {
            // Convert id into an RGB color
            int r = (shape.id & 0x000000FF) >>  0;
            int g = (shape.id & 0x0000FF00) >>  8;
            int b = (shape.id & 0x00FF0000) >> 16;

            int locationPC = glGetUniformLocation(shader.id(),  "pickingColor");
            glUniform3f(locationPC, r/255.0f, g/255.0f, b/255.0f);
        } else {
            int locationC = glGetUniformLocation(shader.id(),  "ourColor");
            glUniform3f(locationC, shape.r, shape.g, shape.b);
        }

        glDrawArrays(GL_TRIANGLES, 0, shape.s.coords.size() * sizeof(float));
    }
}

void drawCircles (Shader shader) {
    //Draw all circles
    for( circleStruct shape : circleList) {
        glBindVertexArray(shape.VAO);
        //set the transformation for this shape
        Uniform::set(shader.id(), "model", shape.model);

        if(mouseReleased && edit == MOD) {
            // Convert id into an RGB color
            int r = (shape.id & 0x000000FF) >>  0;
            int g = (shape.id & 0x0000FF00) >>  8;
            int b = (shape.id & 0x00FF0000) >> 16;

            int locationPC = glGetUniformLocation(shader.id(),  "pickingColor");
            glUniform3f(locationPC, r/255.0f, g/255.0f, b/255.0f);
        } else {
            int locationC = glGetUniformLocation(shader.id(),  "ourColor");
            glUniform3f(locationC, shape.r, shape.g, shape.b);
        }

        glDrawArrays(GL_TRIANGLES, 0, shape.s.coords.size() * sizeof(float));
    }
}

void drawCustom (Shader shader) {
    //Draw all custom Shapes
    for( customStruct shape : customList) {
        glBindVertexArray(shape.VAO);
        //set the transformation for this shape
        Uniform::set(shader.id(), "model", shape.model);

        if(mouseReleased && edit == MOD) {
            // Convert id into an RGB color
            int r = (shape.id & 0x000000FF) >>  0;
            int g = (shape.id & 0x0000FF00) >>  8;
            int b = (shape.id & 0x00FF0000) >> 16;

            int locationPC = glGetUniformLocation(shader.id(),  "pickingColor");
            glUniform3f(locationPC, r/255.0f, g/255.0f, b/255.0f);
        } else {
            int locationC = glGetUniformLocation(shader.id(),  "ourColor");
            glUniform3f(locationC, shape.r, shape.g, shape.b);
        }

        glDrawArrays(GL_TRIANGLES, 0, shape.coords.size() * sizeof(float));
    }
}

int getID (GLFWwindow* window) {
    //Code based on http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-an-opengl-hack/
    //Force all of the object to be drawn and wait for it to finish
    glFlush();
    glFinish();

    //dictate how the pixel will be stored
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    //get mouse position in window
    double xpos, ypos;

    glfwGetCursorPos(window, &xpos, &ypos);

    //get pixel color at specified position
    unsigned char data[4];
    glReadPixels(xpos, SCREEN_HEIGHT - ypos, 1,1, GL_RGBA, GL_UNSIGNED_BYTE, data);

    // Convert the color back to an integer ID
    int pickedID =
            data[0] +
            data[1] * 256 +
            data[2] * 256*256;

    return pickedID;
}

void modSquare(squareStruct * shape) {
    //create new shape to replace old
    squareStruct newShape;
    newShape = *shape;

    char answer;

    //prompt user to input
    std::cout << "Would you like to move the square? Y/N" << std::endl;
    std::cin >> answer;

    Matrix trans;
    Matrix scale;
    Matrix rotate;

    if(answer == 'Y' || answer == 'y') {
        float xT, yT;
        std::cout << "Enter a point to center square at, with (0,0) as the center of the window: X,Y " << std::endl;
        std::cin >> xT;
        std::cin.ignore(1, ',');
        std::cin >> yT;

        trans.translate(xT, yT, 0);
    }

    newShape.model = trans * newShape.model;

    std::cout << "Would you like to scale the square? Y/N"  << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float xS, yS;
        std::cout << "Enter a scale for the X and Y dimension of the square: X-scale, Y-scale " << std::endl;
        std::cin >> xS;
        std::cin.ignore(1, ',');
        std::cin >> yS;

        scale.scale(shape->xS * xS, shape->xS * yS, 1.0);
    }

    newShape.model = scale * newShape.model;

    std::cout << "Would you like to rotate the square? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float r;
        std::cout << "Enter a rotation for the square in degrees: " << std::endl;
        std::cin >> r;

        rotate.rotate_z(r + shape->rot);
    }

    newShape.model = rotate * newShape.model;

    std::cout << "Would you like to change the square's color? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float rC, gC, bC;
        std::cout << "Enter 3 numbers between 0 and 1: R,G,B" << std::endl;
        std::cin >> rC;
        std::cin.ignore(1, ',');
        std::cin >> gC;
        std::cin.ignore(1, ',');
        std::cin >> bC;

        newShape.r = rC;
        newShape.g = gC;
        newShape.b = bC;
    }

    squareList.remove(*shape);
    squareList.push_back(newShape);
}

void modTriangle(triangleStruct * shape) {
    //create new shape to replace old
    triangleStruct newShape;
    newShape = *shape;

    char answer;

    //prompt user to input
    std::cout << "Would you like to move the triangle? Y/N" << std::endl;
    std::cin >> answer;

    Matrix trans;
    Matrix scale;
    Matrix rotate;

    if(answer == 'Y' || answer == 'y') {
        float xT, yT;
        std::cout << "Enter a point to center triangle at, with (0,0) as the center of the window: X,Y " << std::endl;
        std::cin >> xT;
        std::cin.ignore(1, ',');
        std::cin >> yT;

        trans.translate(xT, yT, 0);
    }

    newShape.model = trans * newShape.model;

    std::cout << "Would you like to scale the triangle? Y/N"  << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float xS, yS;
        std::cout << "Enter a scale for the X and Y dimension of the triangle: X-scale, Y-scale " << std::endl;
        std::cin >> xS;
        std::cin.ignore(1, ',');
        std::cin >> yS;

        scale.scale(shape->xS * xS, shape->xS * yS, 1.0);
    }

    newShape.model = scale * newShape.model;

    std::cout << "Would you like to rotate the triangle? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float r;
        std::cout << "Enter a rotation for the triangle in degrees: " << std::endl;
        std::cin >> r;

        rotate.rotate_z(r + shape->rot);
    }

    newShape.model = rotate * newShape.model;

    std::cout << "Would you like to change the triangle's color? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float rC, gC, bC;
        std::cout << "Enter 3 numbers between 0 and 1: R,G,B" << std::endl;
        std::cin >> rC;
        std::cin.ignore(1, ',');
        std::cin >> gC;
        std::cin.ignore(1, ',');
        std::cin >> bC;

        newShape.r = rC;
        newShape.g = gC;
        newShape.b = bC;
    }

    triangleList.remove(*shape);
    triangleList.push_back(newShape);

}

void modCircle(circleStruct * shape) {
    //create new shape to replace old
    circleStruct newShape;
    newShape = *shape;

    char answer;

    //prompt user to input
    std::cout << "Would you like to move the circle? Y/N" << std::endl;
    std::cin >> answer;

    Matrix trans;
    Matrix scale;
    Matrix rotate;

    if(answer == 'Y' || answer == 'y') {
        float xT, yT;
        std::cout << "Enter a point to center circle at, with (0,0) as the center of the window: X,Y " << std::endl;
        std::cin >> xT;
        std::cin.ignore(1, ',');
        std::cin >> yT;

        trans.translate(xT, yT, 0);
    }

    newShape.model = trans * newShape.model;

    std::cout << "Would you like to scale the circle? Y/N"  << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float xS, yS;
        std::cout << "Enter a scale for the X and Y dimension of the circle: X-scale, Y-scale " << std::endl;
        std::cin >> xS;
        std::cin.ignore(1, ',');
        std::cin >> yS;

        scale.scale(shape->xS * xS, shape->xS * yS, 1.0);
    }

    newShape.model = scale * newShape.model;

    std::cout << "Would you like to rotate the circle? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float r;
        std::cout << "Enter a rotation for the circle in degrees: " << std::endl;
        std::cin >> r;

        rotate.rotate_z(r + shape->rot);
    }

    newShape.model = rotate * newShape.model;

    std::cout << "Would you like to change the circle's color? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float rC, gC, bC;
        std::cout << "Enter 3 numbers between 0 and 1: R,G,B" << std::endl;
        std::cin >> rC;
        std::cin.ignore(1, ',');
        std::cin >> gC;
        std::cin.ignore(1, ',');
        std::cin >> bC;

        newShape.r = rC;
        newShape.g = gC;
        newShape.b = bC;
    }

    circleList.remove(*shape);
    circleList.push_back(newShape);
}

void modCustom(customStruct * shape) {
    //create new shape to replace old
    customStruct newShape;
    newShape = *shape;

    char answer;

    //prompt user to input
    std::cout << "Would you like to move the shape? Y/N" << std::endl;
    std::cin >> answer;

    Matrix trans;
    Matrix scale;
    Matrix rotate;

    if(answer == 'Y' || answer == 'y') {
        float xT, yT;
        std::cout << "Enter a point to center the shape at, with (0,0) as the center of the window: X,Y " << std::endl;
        std::cin >> xT;
        std::cin.ignore(1, ',');
        std::cin >> yT;

        trans.translate(xT, yT, 0);
    }

    newShape.model = trans * newShape.model;

    std::cout << "Would you like to scale the shape? Y/N"  << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float xS, yS;
        std::cout << "Enter a scale for the X and Y dimension of the shape: X-scale, Y-scale " << std::endl;
        std::cin >> xS;
        std::cin.ignore(1, ',');
        std::cin >> yS;

        scale.scale(shape->xS * xS, shape->xS * yS, 1.0);
    }

    newShape.model = scale * newShape.model;

    std::cout << "Would you like to rotate the shape? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float r;
        std::cout << "Enter a rotation for the shape in degrees: " << std::endl;
        std::cin >> r;

        rotate.rotate_z(r + shape->rot);
    }

    newShape.model = rotate * newShape.model;

    std::cout << "Would you like to change the shape's color? Y/N" << std::endl;
    std::cin >> answer;

    if(answer == 'Y' || answer == 'y') {
        float rC, gC, bC;
        std::cout << "Enter 3 numbers between 0 and 1: R,G,B" << std::endl;
        std::cin >> rC;
        std::cin.ignore(1, ',');
        std::cin >> gC;
        std::cin.ignore(1, ',');
        std::cin >> bC;

        newShape.r = rC;
        newShape.g = gC;
        newShape.b = bC;
    }

    customList.remove(*shape);
    customList.push_back(newShape);
}

int main() {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "program1", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // set the matrices
    Matrix model;

    // setup projection
    Matrix projection;
    projection.ortho(SCREEN_WIDTH/SCREEN_HEIGHT, -SCREEN_WIDTH/SCREEN_HEIGHT, -1.0f, 1.0f, 0.01f, 10.0f);

    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetKeyCallback(window, key_callback);

    model.translate(0.2, 0.2, 0.2);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(model, window);

        // setup view, and allow the camera to move around the world
        Vector eye(camX, camY, -1);
        Vector target(camX, camY, 0);
        Vector up(0, 1, 0);

        Matrix camera;
        camera.look_at(eye, target, up);

        glClearColor(1.0, 1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (mouseReleased && edit == MOD) {
            int locationP = glGetUniformLocation(shader.id(), "picking");
            glUniform1i(locationP, mouseReleased);
        }

        //Draw the shapes
        drawSquares(shader);
        drawTriangles(shader);
        drawCircles(shader);
        drawCustom(shader);

        //get new window size, supposedly
        int width, height;
        glfwGetWindowSize(window, &width, &height);

        projection.ortho(width/height*zoom, -width/height*zoom, -1.0f*zoom, 1.0f*zoom, 0.01f, 10.0f);
        Uniform::set(shader.id(), "projection", projection);
        Uniform::set(shader.id(), "camera", camera);

        if (mouseReleased && edit == MOD) {
            //Get the id for the shape picked
            int pickedID = getID(window);

            bool shapeFound = false;
            std::cout << "Picked ID: " << pickedID << std::endl;
            if (pickedID == 0x00ffffff){ // Full white, must be the background !
                std::cout << "You selected the background." << std::endl;
            } else {
                //iterate through squares looking for ID
                for (squareStruct shape : squareList) {
                    if (pickedID == shape.id) {
                        std::cout << "Selected square with id: " << shape.id << std::endl;
                        shapeFound = true;
                        modSquare(&shape);
                        break;
                    }
                }
                //iterate through triangles looking for ID
                if (!shapeFound) {
                    for (triangleStruct shape : triangleList) {
                        if (pickedID == shape.id) {
                            std::cout << "Selected triangle with id: " << shape.id << std::endl;
                            shapeFound = true;
                            modTriangle(&shape);
                            break;
                        }
                    }
                }
                //iterate through circles looking for ID
                if (!shapeFound) {
                    for (circleStruct shape : circleList) {
                        if (pickedID == shape.id) {
                            std::cout << "Selected circle with id: " << shape.id << std::endl;
                            shapeFound = true;
                            modCircle(&shape);
                            break;
                        }
                    }
                }
                if (!shapeFound) {
                    for (customStruct shape : customList) {
                        if (pickedID == shape.id) {
                            std::cout << "Selected circle with id: " << shape.id << std::endl;
                            shapeFound = true;
                            modCustom(&shape);
                            break;
                        }
                    }
                }
            }

            //Reset uniform variable
            int locationP = glGetUniformLocation(shader.id(),  "picking");
            glUniform1i(locationP, false);

        } else if(mousePressed && view) {
            double xpos, ypos;

            glfwGetCursorPos(window, &xpos, &ypos);

            Vector current(xpos, ypos, 0);
            windowCurrent = current;

            convertAndSendCameraCoords();

            //Keep the movement from continuing when mouse is still
            Vector previous (xpos, ypos, 0);
            windowPress = previous;
        }


        //mouse input reset
        mouseReleased = false;

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    };

    glfwTerminate();
    return 0;
}