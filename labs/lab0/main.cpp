#include <iostream>
#include <string>

class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }

    Vector3 () {
        x = 0;
        y = 0;
        z = 0;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 add(Vector3& v, Vector3& v2) { // v and v2 are copies, so any changes to them in this function
                                     // won't affect the originals
    Vector3 result((v.x + v2.x), (v.y + v2.y), (v.z + v2.z));

    return result;
}

Vector3 operator+(Vector3& v, Vector3& v2) {
    Vector3 result((v.x + v2.x), (v.y + v2.y), (v.z + v2.z));

    return result;
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector

    stream << "x=" << v.x << " y= " << v.y << " z= " << v.z;

    return stream;
}

int main(int argc, char** argv) {
    Vector3 a(1,2,3);
    Vector3 b(4,5,6);

    std::cout << a+b << std::endl;
}
