To enter the View mode, press v.

To enter the Edit mode, press e.

While in View mode:

    click and drag on the window to move around the world,

    press - to zoom out of the world,
    
    press = to zoom in on the world

While in the Edit mode:
    
    press 1 to enter stamp mode,
    
    press 2 to enter user defined primitive mode,
    
    press 3 to enter group mode,
    
    press 4 to enter modify mode,

While in Stamp sub-mode:
    
    press s to make a square, then follow command line prompts
    
    press t to make a triangle, then follow command line prompts
    
    press c to make a circle, then follow command line prompts

While in User Defined Primitive sub-mode:
    
    press c to begin making a shape, then follow command line prompts

While in Group sub-mode:
    
    ran out of time to implement

While in Modify Mode:
    
    click on a shape, then follow command line prompts to modify it


The shape I made is rather simple. This would allow for it to be hardcoded,
like a square or cube, making testing with it in the future easier. It should make
for a good test object because of its indent in the top, that would allow for
testing a more complicated light system where light can be blocked, and make
for a more complicated physics interaction because of its angled surfaces that can
be easily adjusted by changing the inside point, or making it wider.