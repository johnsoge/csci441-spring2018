#version 330 core
in vec3 FragPosition;
in vec3 ourColor;
in vec3 normal;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
uniform vec3 lightPos;
uniform vec3 cameraPos;

vec3 color;

out vec4 fragColor;

void main() {
    //Ambient Light
    float amb = 0.1;
    vec3 ambient = amb * vec3(1.0, 1.0, 1.0);

    //Diffuse Light
    vec3 norm = normalize(normal);
    vec3 light = vec3(camera * model * vec4(lightPos, 1.0));
    vec3 lightDir = normalize(light - FragPosition);

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * vec3(1.0, 1.0, 1.0);

    //Specular Light
    float specularStrength = 0.5;
    vec3 viewDir = normalize(cameraPos - FragPosition);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * vec3(1.0, 1.0, 1.0);

    color = (ambient + diffuse*0.5 + specular) * ourColor;

    fragColor = vec4(color, 1.0f);
}
