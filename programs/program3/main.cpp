#include <iostream>
#include <list>
#include <ctime>

#include <glm/glm.hpp>

#include <csci441/vector.h>

#include "bitmap_image.hpp"
#include "shape.h"

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Ray {
    glm::vec3 origin;
    glm::vec3 dir;
};

struct Face {
    Vector a = Vector(0,0,0);
    Vector b = Vector(0,0,0);
    Vector c = Vector(0,0,0);
    glm::vec3 color;
    Vector normal = Vector(0,0,0);
};

glm::vec3 getColor(glm::vec3 color, Vector inNormal, glm::vec3 pos, Ray ray) {
    glm::vec3 lightPos (3.75f, 3.75f, -4.0f);
    glm::vec3 lightColor (1.0,1.0,1.0);

    //calculate ambient light
    float ambientStrength = 0.2;
    glm::vec3 ambient = ambientStrength * lightColor;

    //calculate diffuse light
    glm::vec3 normal (inNormal.x(), inNormal.y(), inNormal.z());
    normal = glm::normalize(normal);
    glm::vec3 lightDir = glm::normalize(lightPos - pos);
    float diff = glm::max(glm::dot(normal, lightDir), 0.0f);
    glm::vec3 diffuse = diff * lightColor;

    //calculate specular light
    float specularStrength = 0.5;
    glm::vec3 viewDir = glm::normalize(ray.origin - pos);
    glm::vec3 reflectDir = glm::reflect(-lightDir, normal);
    float spec = glm::pow(glm::max(glm::dot(viewDir, reflectDir), 0.0f), 32);
    glm::vec3 specular = diffuse* specularStrength * spec * lightColor;


    return (ambient + diffuse + specular) * color;
}

glm::vec3 intersect (Ray ray, const std::vector< Face >& world) {
    glm::vec3 color = glm::vec3 (75,156,211);
    float t = 1000;

    for (int i = 0; i < world.size(); i++) {
        //calculate intersection for each face
        glm::mat3 mat = glm::mat3(glm::vec3(world[i].b.x() - world[i].a.x(), world[i].b.y() - world[i].a.y(), world[i].b.z() - world[i].a.z()),
                                  glm::vec3(world[i].c.x() - world[i].a.x(), world[i].c.y() - world[i].a.y(), world[i].c.z() - world[i].a.z()),
                                  glm::vec3(-ray.dir.x, -ray.dir.y, -ray.dir.z));
        float det = glm::determinant(mat);
        glm::vec3 sol = glm::vec3(ray.origin.x - world[i].a.x(), ray.origin.y - world[i].a.y(), ray.origin.z - world[i].a.z());

        //calculate u
        glm::mat3 uMat = glm::mat3(sol,
                                   glm::vec3(world[i].c.x() - world[i].a.x(), world[i].c.y() - world[i].a.y(), world[i].c.z() - world[i].a.z()),
                                   glm::vec3(-ray.dir.x, -ray.dir.y, -ray.dir.z));
        float u = glm::determinant(uMat)/det;

        //calculate v
        glm::mat3 vMat = glm::mat3(glm::vec3(world[i].b.x() - world[i].a.x(), world[i].b.y() - world[i].a.y(), world[i].b.z() - world[i].a.z()),
                                   sol,
                                   glm::vec3(-ray.dir.x, -ray.dir.y, -ray.dir.z));
        float v = glm::determinant(vMat)/det;

        //calculate t
        glm::mat3 tMat = glm::mat3(glm::vec3(world[i].b.x() - world[i].a.x(), world[i].b.y() - world[i].a.y(), world[i].b.z() - world[i].a.z()),
                                   glm::vec3(world[i].c.x() - world[i].a.x(), world[i].c.y() - world[i].a.y(), world[i].c.z() - world[i].a.z()),
                                   sol);
        float t1 = -glm::determinant(tMat)/det;

        //Check if there is an intersection
        if( v < 0 || v > 1) { } //no intersection
        else if ( u < 0 || u > (1-v)) { } //no intersection
        else if ( t < 0 ) { } //no intersection
        else if ( t1 < t ){ //intersection, and make sure it is the closest intersection
            t = t1;
            //calculations for color
            //calculate position
            float x = world[i].a.x() + u*(world[i].b.x() - world[i].a.x()) + v*(world[i].c.x() - world[i].a.x());
            float y = world[i].a.y() + u*(world[i].b.y() - world[i].a.y()) + v*(world[i].c.y() - world[i].a.y());
            float z = world[i].a.z() + u*(world[i].b.z() - world[i].a.z()) + v*(world[i].c.z() - world[i].a.z());
            glm::vec3 pos (x,y,z);

            //std::cout << "t= " << t <<std::endl;

            //calculate color
            color = world[i].color * 250.0f;
            color = getColor(color, world[i].normal, pos, ray);
        }

    }

    return color;
}

void parallel_render(bitmap_image& image, const std::vector< Face >& world) {
    //generate viewport
    float l = -5, r = 5, b = -5, t = 5;
    glm::vec3 cam (1,1,1);

    //found https://bisqwit.iki.fi/story/howto/openmp/
    //runs the for loop in parallel
    #pragma omp parallel for
    for (int x = 0; x <= 640; x++) {
        for(int y = 0; y <= 480; y++) {
            Ray ray;

            //calculate x and y of ray
            float ui = l + (r - l) * (x + 0.5f) / 640;
            float vj = b + (t - b) * (y + 0.5f) / 480;

            /* Equations to generate an orthographic ray
            ray.origin = glm::vec3(ui, vj, -5);
            ray.dir = glm::vec3(0,0,-1);*/

            //generate ray
            ray.origin = glm::vec3(0, 0, -15);
            ray.dir = ray.origin - glm::vec3(ui, vj, 0);
            ray.dir = glm::normalize(ray.dir);

            //std::cout << "Checking for an intersection." << std::endl;

            //calculate intersection
            glm::vec3 color = intersect(ray, world);

            //set the pixel
            image.set_pixel(x, 480 - y, color.x, color.y, color.z);
        }
    }

    //std::cout << "Finished rendering." << std::endl;
}

void render(bitmap_image& image, const std::vector< Face >& world) {
    //generate viewport
    float l = -5, r = 5, b = -5, t = 5;
    glm::vec3 cam (1,1,1);

    for (float x = 0; x <= 640; x++) {
        for (float y = 0; y <= 480; y++) {
            Ray ray;

            //calculate x and y of ray
            float ui = l + (r - l) * (x + 0.5f) / 640;
            float vj = b + (t - b) * (y + 0.5f) / 480;

            /* Equations to generate an orthographic ray
            ray.origin = glm::vec3(ui, vj, -5);
            ray.dir = glm::vec3(0,0,-1);*/

            //generate ray
            ray.origin = glm::vec3(0, 0, -15);
            ray.dir = ray.origin - glm::vec3(ui, vj, 0);
            ray.dir = glm::normalize(ray.dir);

            //std::cout << "Checking for an intersection." << std::endl;

            //calculate intersection
            glm::vec3 color = intersect(ray, world);

            //set the pixel
            image.set_pixel(x, 480 - y, color.x, color.y, color.z);
        }
    }

    //std::cout << "Finished rendering." << std::endl;
}

std::vector< Face > readInOBJ (FILE * file, glm::vec3 color) {
    std::vector< unsigned int > vertexIndices;
    std::vector< Vector > temp_vertices;
    std::list< Face > faces;
    int res;
    //read in obj file
    do {
        char lineHeader[128];
        res = fscanf(file, "%s", lineHeader);

        //Read in vertices;
        if ( strcmp( lineHeader, "v" ) == 0 ) {
            float x, y, z;
            fscanf(file, "%f %f %f\n", &x, &y, &z);
            Vector vertex(x,y,z);
            //std::cout << vertex << std::endl;
            temp_vertices.push_back(vertex);

        }else if ( strcmp( lineHeader, "f" ) == 0 ) {
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0],
                   &vertexIndex[1], &uvIndex[1], &normalIndex[1],
                   &vertexIndex[2], &uvIndex[2], &normalIndex[2] );

            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
        }
    } while(res != EOF);

    //convert obj file to format for opengl
    std::vector< Face > coords;
    for (int i = 0; i < vertexIndices.size(); i++) {
        Face temp;
        temp.a = temp_vertices[vertexIndices[i++]-1];
        temp.b = temp_vertices[vertexIndices[i++]-1];
        temp.c = temp_vertices[vertexIndices[i]-1];

        temp.color = color;

        Vector lineAB = temp.b - temp.a;
        Vector lineAC = temp.c - temp.a;

        Vector normal = lineAB.cross(lineAC);
        normal = normal.normalized();

        temp.normal = normal;

        coords.push_back(temp);
    }

    //std::cout << "Finished reading in OBJ." << std::endl;
    return coords;
}

int main(int argc, char** argv) {

    std::cout << "Starting." << std::endl;

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);
    bitmap_image image2(640, 480);

    FILE * shape = fopen("../models/sphere.obj", "r");
    glm::vec3 colorA (0.722,0.525,0.043);

    // build world
    std::vector< Face > world = readInOBJ(shape, colorA);

    std::clock_t start;
    double duration;

    //render the image using a basic function
    start = std::clock();
    // render the world
    render(image, world);
    duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
    std::cout << "Duration without threading: " << duration << std::endl;

    //render the image using a paralleled function
    start = std::clock();
    parallel_render(image2, world);
    duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
    std::cout << "Duration with threading: " << duration << std::endl;

    image.save_image("non_parallel.bmp");
    image2.save_image("parallel.bmp");
    std::cout << "Success" << std::endl;
}