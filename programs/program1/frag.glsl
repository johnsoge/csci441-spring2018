#version 330 core

uniform vec3 ourColor;

uniform vec3 pickingColor;
uniform bool picking;

out vec4 fragColor;

void main() {
    if (picking) {
        fragColor = vec4(pickingColor, 1.0);
    }
    else {
        fragColor = vec4(ourColor, 1.0f);
    }
}
