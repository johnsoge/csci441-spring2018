#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNormal;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
uniform mat4 inTrModel;

out vec3 FragPosition;
out vec3 ourColor;
out vec3 normal;

void main() {

    gl_Position = projection * camera * model * vec4(aPos, 1.0);

    mat4 inTr = inverse(inTrModel);

    FragPosition = vec3(model * vec4(aPos, 1.0));
    ourColor = aColor;
    normal = (inTr * vec4(aNormal, 1.0)).xyz;
}
