#version 330 core
layout (location = 0) in vec4 aPos;
layout (location = 1) in vec3 aColor;

vec4 someVec;

out vec3 myColor;

uniform vec4 trans1;
uniform vec4 trans2;
uniform vec4 trans3;
uniform vec4 trans4;

void transform () {
    someVec.x = trans1.x * aPos.x + trans1.y * aPos.y + trans1.z * aPos.z + trans1.w * aPos.w;
    someVec.y = trans2.x * aPos.x + trans2.y * aPos.y + trans2.z * aPos.z + trans2.w * aPos.w;
    someVec.z = trans3.x * aPos.x + trans3.y * aPos.y + trans3.z * aPos.z + trans3.w * aPos.w;
    someVec.w = trans4.x * aPos.x + trans4.y * aPos.y + trans4.z * aPos.z + trans4.w * aPos.w;
}

void main() {
    transform();
    gl_Position = vec4(someVec);
    myColor = aColor;
}
