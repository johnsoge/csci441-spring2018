#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

//transformation matrix
float trans[] = {
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0,
};

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void reset () {
    trans[0] = 1.0;
    trans[1] = 0.0;
    trans[2] = 0.0;
    trans[3] = 0.0;
    trans[4] = 0.0;
    trans[5] = 1.0;
    trans[6] = 0.0;
    trans[7] = 0.0;
    trans[8] = 0.0;
    trans[9] = 0.0;
    trans[10] = 1.0;
    trans[11] = 0.0;
    trans[12] = 0.0;
    trans[13] = 0.0;
    trans[14] = 0.0;
    trans[15] = 1.0;
}

int mode;

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

//multiply matrices
void matMult(float mat[]) {
    float result [16] = {
             0, 0, 0, 0,
             0, 0, 0, 0,
             0, 0, 0, 0,
             0, 0, 0, 0
    };

    result[0] = trans[0] * mat[0] + trans[1] * mat[4] + trans[2] * mat[8] + trans[3] * mat[12];
    result[1] = trans[0] * mat[1] + trans[1] * mat[5] + trans[2] * mat[9] + trans[3] * mat[13];
    result[2] = trans[0] * mat[2] + trans[1] * mat[6] + trans[2] * mat[10] + trans[3] * mat[14];
    result[3] = trans[0] * mat[3] + trans[1] * mat[7] + trans[2] * mat[11] + trans[3] * mat[15];
    result[4] = trans[4] * mat[0] + trans[5] * mat[4] + trans[6] * mat[8] + trans[7] * mat[12];
    result[5] = trans[4] * mat[1] + trans[5] * mat[5] + trans[6] * mat[9] + trans[7] * mat[13];
    result[6] = trans[4] * mat[2] + trans[5] * mat[6] + trans[6] * mat[10] + trans[7] * mat[14];
    result[7] = trans[4] * mat[3] + trans[5] * mat[7] + trans[6] * mat[11] + trans[7] * mat[15];
    result[8] = trans[8] * mat[0] + trans[9] * mat[4] + trans[10] * mat[8] + trans[11] * mat[12];
    result[9] = trans[8] * mat[1] + trans[9] * mat[5] + trans[10] * mat[9] + trans[11] * mat[13];
    result[10] = trans[8] * mat[2] + trans[9] * mat[6] + trans[10] * mat[10] + trans[11] * mat[14];
    result[11] = trans[8] * mat[3] + trans[9] * mat[7] + trans[10] * mat[11] + trans[11] * mat[15];
    result[12] = trans[12] * mat[0] + trans[13] * mat[4] + trans[14] * mat[8] + trans[15] * mat[12];
    result[13] = trans[12] * mat[1] + trans[13] * mat[5] + trans[14] * mat[9] + trans[15] * mat[13];
    result[14] = trans[12] * mat[2] + trans[13] * mat[6] + trans[14] * mat[10] + trans[15] * mat[14];
    result[15] = trans[12] * mat[3] + trans[13] * mat[7] + trans[14] * mat[11] + trans[15] * mat[15];

    trans[0] = result[0];
    trans[1] = result[1];
    trans[2] = result[2];
    trans[3] = result[3];
    trans[4] = result[4];
    trans[5] = result[5];
    trans[6] = result[6];
    trans[7] = result[7];
    trans[8] = result[8];
    trans[9] = result[9];
    trans[10] = result[10];
    trans[11] = result[11];
    trans[12] = result[12];
    trans[13] = result[13];
    trans[14] = result[14];
    trans[15] = result[15];
}

//look for spacebar releasae
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE) {
        if (mode == 0) {
            mode = 1;
        }
        else if(mode == 1) {
            mode = 2;
        }
        else if(mode == 2){
            mode = 3;
        }
        else if(mode == 3)
        {
            mode = 4;
        } else{
            mode = 0;
        }
    }
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 0.0f, 1.0f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 0.0f, 1.0f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0f, 1.0f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 0.0f, 1.0f,1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0f, 1.0f,0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0f, 1.0f,0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 7*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 7*sizeof(float), (void*)(4*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    mode = 0;
    glfwSetKeyCallback(window, key_callback);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */

        //rotation matrix
        float rotate[] = {
                (float) cos(glfwGetTime()), (float) -sin(glfwGetTime()), 0.0, 0.0,
                (float) sin(glfwGetTime()), (float) cos(glfwGetTime()), 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 1.0
        };

        //scale matrix
        float scale[] = {
            (float) sin(glfwGetTime()), 0.0, 0.0, 0.0,
                    0.0, (float) sin(glfwGetTime()), 0.0, 0.0,
                    0.0, 0.0, 1.0, 0.0,
                    0.0, 0.0, 0.0, 1.0
        };

        //translation matrix
        float translate[] = {
                1.0, 0.0, 0.0, (float) sin(glfwGetTime()),
                0.0, 1.0, 0.0, (float) sin(glfwGetTime()),
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 1.0
        };

        //matMult(rotate);

        if(mode == 0)
        {
            int location1 = glGetUniformLocation(shader.id(), "trans1");
            glUniform4f(location1, trans[0], trans[1], trans[2], trans[3]);
            int location2 = glGetUniformLocation(shader.id(), "trans2");
            glUniform4f(location2, trans[4], trans[5], trans[6], trans[7]);
            int location3 = glGetUniformLocation(shader.id(), "trans3");
            glUniform4f(location3, trans[8], trans[9], trans[10], trans[11]);
            int location4 = glGetUniformLocation(shader.id(), "trans4");
            glUniform4f(location4, trans[12], trans[13], trans[14], trans[15]);
        }
        else if (mode == 1)
        {
            int location1 = glGetUniformLocation(shader.id(), "trans1");
            glUniform4f(location1, rotate[0], rotate[1], rotate[2], rotate[3]);
            int location2 = glGetUniformLocation(shader.id(), "trans2");
            glUniform4f(location2, rotate[4], rotate[5], rotate[6], rotate[7]);
            int location3 = glGetUniformLocation(shader.id(), "trans3");
            glUniform4f(location3, rotate[8], rotate[9], rotate[10], rotate[11]);
            int location4 = glGetUniformLocation(shader.id(), "trans4");
            glUniform4f(location4, rotate[12], rotate[13], rotate[14], rotate[15]);
        }
        else if (mode == 2)
        {
            int location1 = glGetUniformLocation(shader.id(), "trans1");
            glUniform4f(location1, scale[0], scale[1], scale[2], scale[3]);
            int location2 = glGetUniformLocation(shader.id(), "trans2");
            glUniform4f(location2, scale[4], scale[5], scale[6], scale[7]);
            int location3 = glGetUniformLocation(shader.id(), "trans3");
            glUniform4f(location3, scale[8], scale[9], scale[10], scale[11]);
            int location4 = glGetUniformLocation(shader.id(), "trans4");
            glUniform4f(location4, scale[12], scale[13], scale[14], scale[15]);
        }
        else if (mode == 3)
        {
            //translation matrix
            float translate2[] = {
                    1.0, 0.0, 0.0, (float) 1.0,
                    0.0, 1.0, 0.0, (float) 0.5,
                    0.0, 0.0, 1.0, 0.0,
                    0.0, 0.0, 0.0, 1.0
            };
            //scale matrix
            float scale2[] = {
                    (float) 0.5, 0.0, 0.0, 0.0,
                    0.0, (float) 0.5, 0.0, 0.0,
                    0.0, 0.0, 1.0, 0.0,
                    0.0, 0.0, 0.0, 1.0
            };
            matMult(scale2);
            matMult(translate2);
            matMult(rotate);
            matMult(translate);
            matMult(rotate);
            int location1 = glGetUniformLocation(shader.id(), "trans1");
            glUniform4f(location1, trans[0], trans[1], trans[2], trans[3]);
            int location2 = glGetUniformLocation(shader.id(), "trans2");
            glUniform4f(location2, trans[4], trans[5], trans[6], trans[7]);
            int location3 = glGetUniformLocation(shader.id(), "trans3");
            glUniform4f(location3, trans[8], trans[9], trans[10], trans[11]);
            int location4 = glGetUniformLocation(shader.id(), "trans4");
            glUniform4f(location4, trans[12], trans[13], trans[14], trans[15]);
        }
        else if (mode == 4){
            matMult(scale);
            matMult(rotate);
            matMult(translate);
            matMult(scale);
            matMult(rotate);
            int location1 = glGetUniformLocation(shader.id(), "trans1");
            glUniform4f(location1, trans[0], trans[1], trans[2], trans[3]);
            int location2 = glGetUniformLocation(shader.id(), "trans2");
            glUniform4f(location2, trans[4], trans[5], trans[6], trans[7]);
            int location3 = glGetUniformLocation(shader.id(), "trans3");
            glUniform4f(location3, trans[8], trans[9], trans[10], trans[11]);
            int location4 = glGetUniformLocation(shader.id(), "trans4");
            glUniform4f(location4, trans[12], trans[13], trans[14], trans[15]);
        }
        else {
            int location1 = glGetUniformLocation(shader.id(), "trans1");
            glUniform4f(location1, trans[0], trans[1], trans[2], trans[3]);
            int location2 = glGetUniformLocation(shader.id(), "trans2");
            glUniform4f(location2, trans[4], trans[5], trans[6], trans[7]);
            int location3 = glGetUniformLocation(shader.id(), "trans3");
            glUniform4f(location3, trans[8], trans[9], trans[10], trans[11]);
            int location4 = glGetUniformLocation(shader.id(), "trans4");
            glUniform4f(location4, trans[12], trans[13], trans[14], trans[15]);
        }

        reset();

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
