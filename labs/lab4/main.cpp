#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//transformation matrix
GLfloat model[4][4] = {
        { 1.0, 0.0, 0.0, 0.0 },
        { 0.0, 1.0, 0.0, 0.0 },
        { 0.0, 0.0, 1.0, 0.0 },
        { 0.0, 0.0, 0.0, 1.0 }
};

//view matrix
GLfloat view[4][4] = {
        { 1.0, 0.0, 0.0, 0.0 },
        { 0.0, 1.0, 0.0, 0.0 },
        { 0.0, 0.0, 1.0, 0.0 },
        { 0.0, 0.0, 0.0, 1.0 }
};
//projection matrix
GLfloat project[4][4] = {
        { 1.0, 0.0, 0.0, 0.0 },
        { 0.0, 1.0, 0.0, 0.0 },
        { 0.0, 0.0, 1.0, 0.0 },
        { 0.0, 0.0, 0.0, 1.0 }
};

float x = 0.0, y = 0.0, z = 0.0, xRotate = 1.0, yRotate = 0.0, zRotate = 0.0, scaleM = 1.0, camY = 0.0;
int projectionMode = 0;

//multiply matrices
void matMultModel(GLfloat mat[4][4]) {
    float result [16] = {
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
    };

    result[0] = model[0][0] * mat[0][0] + model[0][1] * mat[1][0] + model[0][2] * mat[2][0] + model[0][3] * mat[3][0];
    result[1] = model[0][0] * mat[0][1] + model[0][1] * mat[1][1] + model[0][2] * mat[2][1] + model[0][3] * mat[3][1];
    result[2] = model[0][0] * mat[0][2] + model[0][1] * mat[1][2] + model[0][2] * mat[2][2] + model[0][3] * mat[3][2];
    result[3] = model[0][0] * mat[0][3] + model[0][1] * mat[1][3] + model[0][2] * mat[2][3] + model[0][3] * mat[3][3];
    result[4] = model[1][0] * mat[0][0] + model[1][1] * mat[1][0] + model[1][2] * mat[2][0] + model[1][3] * mat[3][0];
    result[5] = model[1][0] * mat[0][1] + model[1][1] * mat[1][1] + model[1][2] * mat[2][1] + model[1][3] * mat[3][1];
    result[6] = model[1][0] * mat[0][2] + model[1][1] * mat[1][2] + model[1][2] * mat[2][2] + model[1][3] * mat[3][2];
    result[7] = model[1][0] * mat[0][3] + model[1][1] * mat[1][3] + model[1][2] * mat[2][3] + model[1][3] * mat[3][3];
    result[8] = model[2][0] * mat[0][0] + model[2][1] * mat[1][0] + model[2][2] * mat[2][0] + model[2][3] * mat[3][0];
    result[9] = model[2][0] * mat[0][1] + model[2][1] * mat[1][1] + model[2][2] * mat[2][1] + model[2][3] * mat[3][1];
    result[10] = model[2][0] * mat[0][2] + model[2][1] * mat[1][2] + model[2][2] * mat[2][2] + model[2][3] * mat[3][2];
    result[11] = model[2][0] * mat[0][3] + model[2][1] * mat[1][3] + model[2][2] * mat[2][3] + model[2][3] * mat[3][3];
    result[12] = model[3][0] * mat[0][0] + model[3][1] * mat[1][0] + model[3][2] * mat[2][0] + model[3][3] * mat[3][0];
    result[13] = model[3][0] * mat[0][1] + model[3][1] * mat[1][1] + model[3][2] * mat[2][1] + model[3][3] * mat[3][1];
    result[14] = model[3][0] * mat[0][2] + model[3][1] * mat[1][2] + model[3][2] * mat[2][2] + model[3][3] * mat[3][2];
    result[15] = model[3][0] * mat[0][3] + model[3][1] * mat[1][3] + model[3][2] * mat[2][3] + model[3][3] * mat[3][3];

    model[0][0] = result[0];
    model[0][1] = result[1];
    model[0][2] = result[2];
    model[0][3] = result[3];
    model[1][0] = result[4];
    model[1][1] = result[5];
    model[1][2] = result[6];
    model[1][3] = result[7];
    model[2][0] = result[8];
    model[2][1] = result[9];
    model[2][2] = result[10];
    model[2][3] = result[11];
    model[3][0] = result[12];
    model[3][1] = result[13];
    model[3][2] = result[14];
    model[3][3] = result[15];
}

//multiply matrices
void matMultView(GLfloat mat[4][4]) {
    float result [16] = {
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
    };

    result[0] = view[0][0] * mat[0][0] + view[0][1] * mat[1][0] + view[0][2] * mat[2][0] + view[0][3] * mat[3][0];
    result[1] = view[0][0] * mat[0][1] + view[0][1] * mat[1][1] + view[0][2] * mat[2][1] + view[0][3] * mat[3][1];
    result[2] = view[0][0] * mat[0][2] + view[0][1] * mat[1][2] + view[0][2] * mat[2][2] + view[0][3] * mat[3][2];
    result[3] = view[0][0] * mat[0][3] + view[0][1] * mat[1][3] + view[0][2] * mat[2][3] + view[0][3] * mat[3][3];
    result[4] = view[1][0] * mat[0][0] + view[1][1] * mat[1][0] + view[1][2] * mat[2][0] + view[1][3] * mat[3][0];
    result[5] = view[1][0] * mat[0][1] + view[1][1] * mat[1][1] + view[1][2] * mat[2][1] + view[1][3] * mat[3][1];
    result[6] = view[1][0] * mat[0][2] + view[1][1] * mat[1][2] + view[1][2] * mat[2][2] + view[1][3] * mat[3][2];
    result[7] = view[1][0] * mat[0][3] + view[1][1] * mat[1][3] + view[1][2] * mat[2][3] + view[1][3] * mat[3][3];
    result[8] = view[2][0] * mat[0][0] + view[2][1] * mat[1][0] + view[2][2] * mat[2][0] + view[2][3] * mat[3][0];
    result[9] = view[2][0] * mat[0][1] + view[2][1] * mat[1][1] + view[2][2] * mat[2][1] + view[2][3] * mat[3][1];
    result[10] = view[2][0] * mat[0][2] + view[2][1] * mat[1][2] + view[2][2] * mat[2][2] + view[2][3] * mat[3][2];
    result[11] = view[2][0] * mat[0][3] + view[2][1] * mat[1][3] + view[2][2] * mat[2][3] + view[2][3] * mat[3][3];
    result[12] = view[3][0] * mat[0][0] + view[3][1] * mat[1][0] + view[3][2] * mat[2][0] + view[3][3] * mat[3][0];
    result[13] = view[3][0] * mat[0][1] + view[3][1] * mat[1][1] + view[3][2] * mat[2][1] + view[3][3] * mat[3][1];
    result[14] = view[3][0] * mat[0][2] + view[3][1] * mat[1][2] + view[3][2] * mat[2][2] + view[3][3] * mat[3][2];
    result[15] = view[3][0] * mat[0][3] + view[3][1] * mat[1][3] + view[3][2] * mat[2][3] + view[3][3] * mat[3][3];

    view[0][0] = result[0];
    view[0][1] = result[1];
    view[0][2] = result[2];
    view[0][3] = result[3];
    view[1][0] = result[4];
    view[1][1] = result[5];
    view[1][2] = result[6];
    view[1][3] = result[7];
    view[2][0] = result[8];
    view[2][1] = result[9];
    view[2][2] = result[10];
    view[2][3] = result[11];
    view[3][0] = result[12];
    view[3][1] = result[13];
    view[3][2] = result[14];
    view[3][3] = result[15];
}

//multiply matrices
void matMultProjection(GLfloat mat[4][4]) {
    float result [16] = {
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
    };

    result[0] = project[0][0] * mat[0][0] + project[0][1] * mat[1][0] + project[0][2] * mat[2][0] + project[0][3] * mat[3][0];
    result[1] = project[0][0] * mat[0][1] + project[0][1] * mat[1][1] + project[0][2] * mat[2][1] + project[0][3] * mat[3][1];
    result[2] = project[0][0] * mat[0][2] + project[0][1] * mat[1][2] + project[0][2] * mat[2][2] + project[0][3] * mat[3][2];
    result[3] = project[0][0] * mat[0][3] + project[0][1] * mat[1][3] + project[0][2] * mat[2][3] + project[0][3] * mat[3][3];
    result[4] = project[1][0] * mat[0][0] + project[1][1] * mat[1][0] + project[1][2] * mat[2][0] + project[1][3] * mat[3][0];
    result[5] = project[1][0] * mat[0][1] + project[1][1] * mat[1][1] + project[1][2] * mat[2][1] + project[1][3] * mat[3][1];
    result[6] = project[1][0] * mat[0][2] + project[1][1] * mat[1][2] + project[1][2] * mat[2][2] + project[1][3] * mat[3][2];
    result[7] = project[1][0] * mat[0][3] + project[1][1] * mat[1][3] + project[1][2] * mat[2][3] + project[1][3] * mat[3][3];
    result[8] = project[2][0] * mat[0][0] + project[2][1] * mat[1][0] + project[2][2] * mat[2][0] + project[2][3] * mat[3][0];
    result[9] = project[2][0] * mat[0][1] + project[2][1] * mat[1][1] + project[2][2] * mat[2][1] + project[2][3] * mat[3][1];
    result[10] = project[2][0] * mat[0][2] + project[2][1] * mat[1][2] + project[2][2] * mat[2][2] + project[2][3] * mat[3][2];
    result[11] = project[2][0] * mat[0][3] + project[2][1] * mat[1][3] + project[2][2] * mat[2][3] + project[2][3] * mat[3][3];
    result[12] = project[3][0] * mat[0][0] + project[3][1] * mat[1][0] + project[3][2] * mat[2][0] + project[3][3] * mat[3][0];
    result[13] = project[3][0] * mat[0][1] + project[3][1] * mat[1][1] + project[3][2] * mat[2][1] + project[3][3] * mat[3][1];
    result[14] = project[3][0] * mat[0][2] + project[3][1] * mat[1][2] + project[3][2] * mat[2][2] + project[3][3] * mat[3][2];
    result[15] = project[3][0] * mat[0][3] + project[3][1] * mat[1][3] + project[3][2] * mat[2][3] + project[3][3] * mat[3][3];

    project[0][0] = result[0];
    project[0][1] = result[1];
    project[0][2] = result[2];
    project[0][3] = result[3];
    project[1][0] = result[4];
    project[1][1] = result[5];
    project[1][2] = result[6];
    project[1][3] = result[7];
    project[2][0] = result[8];
    project[2][1] = result[9];
    project[2][2] = result[10];
    project[2][3] = result[11];
    project[3][0] = result[12];
    project[3][1] = result[13];
    project[3][2] = result[14];
    project[3][3] = result[15];
}

void reset () {
    model[0][0] = 1.0;
    model[0][1] = 0.0;
    model[0][2] = 0.0;
    model[0][3] = 0.0;
    model[1][1] = 0.0;
    model[1][1] = 1.0;
    model[1][2] = 0.0;
    model[1][3] = 0.0;
    model[2][0] = 0.0;
    model[2][1] = 0.0;
    model[2][2] = 1.0;
    model[2][3] = 0.0;
    model[3][0] = 0.0;
    model[3][1] = 0.0;
    model[3][2] = 0.0;
    model[3][3] = 1.0;

    view[0][0] = 1.0;
    view[0][1] = 0.0;
    view[0][2] = 0.0;
    view[0][3] = 0.0;
    view[1][0] = 0.0;
    view[1][1] = 1.0;
    view[1][2] = 0.0;
    view[1][3] = 0.0;
    view[2][0] = 0.0;
    view[2][1] = 0.0;
    view[2][2] = 1.0;
    view[2][3] = 0.0;
    view[3][0] = 0.0;
    view[3][1] = 0.0;
    view[3][2] = 0.0;
    view[3][3] = 1.0;

    project[0][0] = 1.0;
    project[0][1] = 0.0;
    project[0][2] = 0.0;
    project[0][3] = 0.0;
    project[1][0] = 0.0;
    project[1][1] = 1.0;
    project[1][2] = 0.0;
    project[1][3] = 0.0;
    project[2][0] = 0.0;
    project[2][1] = 0.0;
    project[2][2] = 1.0;
    project[2][3] = 0.0;
    project[3][0] = 0.0;
    project[3][1] = 0.0;
    project[3][2] = 0.0;
    project[3][3] = 1.0;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int pressed = 0;

//look for key releasae
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_UP && action == GLFW_RELEASE) {
        y = y + 0.1f;
    }
    if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE) {
        y = y - 0.1f;
    }
    if (key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
        x = x + 0.1f;
    }
    if (key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
        x = x - 0.1f;
    }
    if (key == GLFW_KEY_COMMA && action == GLFW_RELEASE) {
        z = z + 0.1f;
    }
    if (key == GLFW_KEY_PERIOD && action == GLFW_RELEASE) {
        z = z - 0.1f;
    }
    if (key == GLFW_KEY_MINUS && action == GLFW_RELEASE) {
        scaleM = scaleM - 0.1f;
    }
    if (key == GLFW_KEY_EQUAL && action == GLFW_RELEASE) {
        scaleM = scaleM + 0.1f;
    }
    if (key == GLFW_KEY_U && action == GLFW_RELEASE) {
        xRotate = xRotate + 0.1f;
    }
    if (key == GLFW_KEY_I && action == GLFW_RELEASE) {
        xRotate = xRotate - 0.1f;
    }
    if (key == GLFW_KEY_O && action == GLFW_RELEASE) {
        yRotate = yRotate + 0.1f;
    }
    if (key == GLFW_KEY_P && action == GLFW_RELEASE) {
        yRotate = yRotate - 0.1f;
    }
    if (key == GLFW_KEY_LEFT_BRACKET && action == GLFW_PRESS) {
        pressed = 1;
    }
    if (key == GLFW_KEY_RIGHT_BRACKET && action == GLFW_PRESS) {
        pressed = 1;
    }
    if (key == GLFW_KEY_LEFT_BRACKET && action == GLFW_RELEASE) {
        if(pressed == 1) {
            pressed = 0;
            zRotate = zRotate + 0.1f;
        }
    }
    if (key == GLFW_KEY_RIGHT_BRACKET && action == GLFW_RELEASE) {
        if(pressed == 1) {
            pressed = 0;
            zRotate = zRotate - 0.1f;
        }
    }
    if (key == GLFW_KEY_W && action == GLFW_RELEASE) {
        camY = camY - 0.1f;
    }
    if (key == GLFW_KEY_S && action == GLFW_RELEASE) {
        camY = camY + 0.1f;
    }
    if (key == GLFW_KEY_BACKSLASH && action == GLFW_RELEASE) {
        if(projectionMode == 0)
            projectionMode = 1;
        else
            projectionMode = 0;
    }

}

float normalization (float v [3])
{
    return (float) sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

void crossProduct (float a [3], float b [3], float c [3])
{
    c[0] = a[1]*b[2] - a[2]*b[1];
    c[1] = a[2]*b[0] - a[0]*b[2];
    c[2] = a[0]*b[1] - a[1]*b[0];
}
int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(window, key_callback);

    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };

    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        //rotation matrix for x-axis
        GLfloat rotateX[4][4] = {
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, (float) cos(glfwGetTime() * xRotate), (float) -sin(glfwGetTime() * xRotate), 0.0 },
                { 0.0, (float) sin(glfwGetTime() * xRotate), (float) cos(glfwGetTime() * xRotate), 0.0 },
                { 0.0, 0.0, 0.0, 1.0 }
        };

        //rotation matrix for y-axis
        GLfloat rotateY[4][4] = {
                { (float) cos(yRotate), 0.0, (float) sin(yRotate), 0.0 },
                { 0.0, 1.0, 0.0, 0.0 },
                { (float) -sin(yRotate), 0.0, (float) cos(yRotate), 0.0 },
                { 0.0, 0.0, 0.0, 1.0 }
        };

        //rotation matrix for Z-axis
        GLfloat rotateZ[4][4] = {
                { (float) cos(zRotate), (float) -sin(zRotate), 0.0, 0.0 },
                { (float) sin(zRotate), (float) cos(zRotate), 0.0, 0.0 },
                { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 }
        };

        //scale matrix
        float scale[4][4] = {
                { scaleM, 0.0, 0.0, 0.0 },
                { 0.0, scaleM, 0.0, 0.0 },
                { 0.0, 0.0, scaleM, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 }
        };


        //translation matrix
        float translate[4][4] = {
                { 1.0, 0.0, 0.0, x },
                { 0.0, 1.0, 0.0, y },
                { 0.0, 0.0, 1.0, z },
                { 0.0, 0.0, 0.0, 1.0 }
        };
        //apply model matrix
        matMultModel(translate);

        matMultModel(scale);

        matMultModel(rotateZ);
        matMultModel(rotateY);
        matMultModel(rotateX);

        float gaze [3] = { 0.0, -camY, -0.2f};
        float t [3] = {0.0, 1.0, 0.0};

        float nG = normalization(gaze);

        //init w by doing -g/||g||
        float w [3] = { -gaze[0]/nG, -gaze[1]/nG, -gaze[2]/nG };

        float u [3] = { 0.0, 0.0, 0.0 };

        float v [3] = { 0.0, 0.0, 0.0 };

        //Calculate vector U by getting the cross product of T with W, then dividing all elements by the normalisation
        crossProduct(t, w, u);
        float nTXW = normalization(u);

        for(int i = 0; i < 3; i++)
            u[i] = u[i]/nTXW;

        //Calculate V with WXU
        crossProduct(w, u, v);

        //camera alignment matrix
        GLfloat camAlign [4][4] = {
                { u[0], u[1], u[2], 0.0 },
                { v[0], v[1], v[2], 0.0 },
                { w[0], w[1], w[2], 0.0 },
                { 0.0, 0.0, 0.0, 1.0 }
        };

        matMultView(camAlign);

        //camera translation matrix
        GLfloat camTrans[4][4] = {
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, 1.0, 0.0, camY },
                { 0.0, 0.0, 1.0, -0.20f },
                { 0.0, 0.0, 0.0, 1.0 }
        };

        matMultView(camTrans);

        //orthographic projection matrix
        GLfloat orthog[4][4] = {
                { 2.0f/(1.0f - -1.0f), 0.0, 0.0, -(1.0f + -1.0f)/(1.0f - -1.0f) },
                { 0.0, 2.0f/(1.0f - -1.0f), 0.0, -(1.0f + -1.0f)/(1.0f - -1.0f) },
                { 0.0, 0.0, -2/(-1.0f + -1.0f),  -(1.0f + -1.0f)/(-1.0f + -1.0f) },
                { 0.0, 0.0, 0.0, 1.0 }
        };

        matMultProjection(orthog);

        if (projectionMode == 1) {
            //Persepective projection matrix
            GLfloat per [4][4] = {
                    { -0.9f, 0.0, 0.0, 0.0 },
                    { 0.0, -0.9f, 0.0, 0.0 },
                    { 0.0, 0.0, -0.9f + -1.0f, -(-1.0f*-0.9f) },
                    { 0.0, 0.0, -1.0, 0.0 }
            };

            matMultProjection(per);
        }

        matMultView(project);

        //pass model matrix to shader
        int locationM = glGetUniformLocation(shader.id(),  "model");
        glUniformMatrix4fv(locationM, 1, GL_TRUE, &model[0][0]);

        //pass view matrix to shader
        int locationV = glGetUniformLocation(shader.id(),  "view");
        glUniformMatrix4fv(locationV, 1, GL_TRUE, &view[0][0]);

        reset();

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
