#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct Ray {
    glm::vec3 origin;
    glm::vec3 dir;
};

glm::vec3 intersect (Ray ray, const std::vector<Sphere>& world) {
    glm::vec3 color = glm::vec3 (75,156,211);
    float t = 1000;

    for (int i = 0; i < world.size(); i++) {
        //calculate A, B, C of the quadratic equation
        glm::vec3 v = ray.origin - world[i].center;
        float A = ray.dir.x * ray.dir.x +
                  ray.dir.y * ray.dir.y +
                  ray.dir.z * ray.dir.z;

        float B = (ray.dir.x * 2.0f) * (v.x) +
                  (ray.dir.y * 2.0f) * (v.y) +
                  (ray.dir.z * 2.0f) * (v.z);

        float C = (ray.origin.x - world[i].center.x) * (ray.origin.x - world[i].center.x) +
                  (ray.origin.y - world[i].center.y) * (ray.origin.y - world[i].center.y) +
                  (ray.origin.z - world[i].center.z) * (ray.origin.z - world[i].center.z)
                  - world[i].radius * world[i].radius;

        //Make a check if the points are on a sphere
        if((B*B - 4*A*C) < 0) {
            continue;
        } else {
            float q = -(B + B/B * sqrt(B*B - 4.0*A*C));

            float t1 = -q/(2*A);
            float t2 = -(2*C)/q;

            //assign the color if the sphere is the first hit
            if(t > t1) {
                t = t1;
                color = world[i].color * 250.0f;
            }
        }
    }
    return color;
}

void render(bitmap_image& image, const std::vector<Sphere>& world) {
    float l = -5, r = 5, b = -5, t = 5;
    glm::vec3 cam (1,1,1);

    for (float x = 0; x <= 640; x++)
        for (float y = 0; y <= 480; y++)
        {
            Ray ray;

            float ui = l + (r-l)*(x+0.5)/640;
            float vj = b + (t-b)*(y+0.5)/480;

            /* Equations to generate an orthographic ray
            ray.origin = glm::vec3(ui, vj, 0);
            ray.dir = glm::vec3(0,0,-1);*/

            ray.origin = glm::vec3 (0,0,-5);
            ray.dir = ray.origin - glm::vec3(ui, vj, 0);
            ray.dir = glm::normalize(ray.dir);

            glm::vec3 color = intersect(ray, world);

            image.set_pixel(x,480-y,color.x, color.y, color.z);
        }
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
    };

    // render the world
    render(image, world);

    image.save_image("perspective.bmp");
    std::cout << "Success" << std::endl;
}


