#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>


template <typename T, typename N>
void add_vertex(T& coords, const N& x, const N& y) {
    coords.push_back(x);
    coords.push_back(y);
}

class Square {
public:
    std::vector<float> coords;
    Square() : coords{
         -0.5f, -0.5f,
         0.5f, -0.5f,
         0.5f,  0.5f,
         0.5f,  0.5f,
         -0.5f,  0.5f,
         -0.5f, -0.5f
    } {}

};

class Triangle {
public:
    std::vector<float> coords;
    Triangle() : coords{
        -0.5f,  -0.5f,
         0.5f, -0.5f,
         0.5f,  0.5f,
    } {}

};
class Circle {
    double x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    double y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

public:
    std::vector<float> coords;
    Circle(unsigned int n, float radius) {
        int n_steps = (n%2==0) ? n : n+1;
        double step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps; ++i) {
            for (int j = 0; j < n_steps/2.0; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n_steps)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                double vij_x = x(radius, phi_i, theta_j);
                double vij_y = y(radius, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(radius, phi_ip1, theta_j);
                double vip1j_y = y(radius, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(radius, phi_i, theta_jp1);
                double vijp1_y = y(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                double vip1jp1_y = y(radius, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y);
                add_vertex(coords, vip1j_x, vip1j_y);
                add_vertex(coords, vijp1_x, vijp1_y);

                // add triangle
                add_vertex(coords, vijp1_x, vijp1_y);
                add_vertex(coords, vip1jp1_x, vip1jp1_y);
                add_vertex(coords, vip1j_x, vip1j_y);
            }
        }
    }
};
#endif
