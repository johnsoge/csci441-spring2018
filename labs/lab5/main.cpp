#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

//Function to transpose a given matrix
Matrix transpose (Matrix mat) {
    Matrix result;
    int index = 0;
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 16; j+=4) {
            result.values[index] = mat.values[i + j];

            //std::cout << result << std::endl;

            index++;
        }
    }
    return result;
}

unsigned int idx(unsigned int r, unsigned int c) {
    return r + c*4;
}

//Function to inverse a given matrix, using Gauss Jordan
Matrix inverse (Matrix mat) {
    Matrix inverse;

    float mult = 0;

    //get first column first row equal to 1, and apply to inverse matrix

    int pivotRow = 0;
    int pivot = 0;

    for (int i = pivotRow; i < pivotRow + 4; i++) {
        mat.values[i] = mat.values[i]/mat.values[pivot];
        inverse.values[i] = inverse.values[i]/mat.values[pivot];
    }

    //std::cout << "After Division" << std::endl << inverse << std::endl;

    //get first column into upper triangle form
    //row control
    for (int i = 0; i < 16; i+=4) {

        if(i != pivotRow) {

            //set multiplier for row
            mult = mat.values[i];

            //column control
            for (int j = 0; j < 4; j++) {
                //subtract value in first row from rest
                mat.values[i+j] = mat.values[i+j] - mat.values[pivotRow + j] * mult;
                inverse.values[i+j] = inverse.values[i+j] - mat.values[pivotRow + j] * mult;
            }
        }
    }

    //get second column second row equal to 1, and apply to inverse matrix

    pivotRow = 4;
    pivot = 5;

    for (int i = pivotRow; i < pivotRow + 4; i++) {
        mat.values[i] = mat.values[i]/mat.values[pivot];
        inverse.values[i] = inverse.values[i]/mat.values[pivot];
    }
    //std::cout << "After Division" << std::endl << inverse << std::endl;

    //get first column into upper triangle form
    //row control
    for (int i = 0; i < 16; i+=4) {

        if(i != pivotRow) {

            //set multiplier for row
            mult = mat.values[i+1];

            //column control
            for (int j = 0; j < 4; j++) {
                //subtract value in first row from rest
                mat.values[i+j] = mat.values[i+j] - mat.values[pivotRow + j] * mult;
                inverse.values[i+j] = inverse.values[i+j] - mat.values[pivotRow + j] * mult;
            }
        }
    }

    //get third column third row equal to 1, and apply to inverse matrix

    pivotRow = 8;
    pivot = 10;
    for (int i = pivotRow; i < pivotRow + 4; i++) {
        mat.values[i] = mat.values[i]/mat.values[pivot];
        inverse.values[i] = inverse.values[i]/mat.values[pivot];
    }
    //std::cout << "After Division" << std::endl << inverse << std::endl;

    //get first column into upper triangle form
    //row control
    for (int i = 0; i < 16; i+=4) {

        if(i != pivotRow) {

            //set multiplier for row
            mult = mat.values[i+2];

            //column control
            for (int j = 0; j < 4; j++) {
                //subtract value in first row from rest
                mat.values[i+j] = mat.values[i+j] - mat.values[pivotRow + j] * mult;
                inverse.values[i+j] = inverse.values[i+j] - mat.values[pivotRow + j] * mult;
            }
        }
    }

    //get third column third row equal to 1, and apply to inverse matrix

    pivotRow = 12;
    pivot = 15;
    for (int i = pivotRow; i < pivotRow + 4; i++) {
        mat.values[i] = mat.values[i]/mat.values[pivot];
        inverse.values[i] = inverse.values[i]/mat.values[pivot];
    }
    //std::cout << "After Division" << std::endl << inverse << std::endl;

    //get first column into upper triangle form
    //row control
    for (int i = 0; i < 16; i+=4) {

        if(i != pivotRow) {

            //set multiplier for row
            mult = mat.values[i+3];

            //column control
            for (int j = 0; j < 4; j++) {
                //subtract value in first row from rest
                mat.values[i+j] = mat.values[i+j] - mat.values[pivotRow + j] * mult;
                inverse.values[i+j] = inverse.values[i+j] - mat.values[pivotRow + j] * mult;
            }
        }
    }

    //std::cout << "Final Matrix" << std::endl << inverse << std::endl;

    return inverse;
}

int mode;

//look for spacebar releasae
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE) {
        if (mode == 0) {
            mode = 1;
            std::cout << "Change" << std::endl;
        }
        else {
            mode = 0;
            std::cout << "Change" << std::endl;
        }
    }
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "lab5", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }


    // create c
    // Cylinder c(20, 1, .2, .4);
    // Cone c(20, 1, .2, .4);
    Sphere s(20, .5, 1, .2, .4);
    // Torus c(20, .75, .25, 1, .2, .4);
    DiscoCube c;

    // copy vertex data for cube
    GLuint VBO1;
    glGenBuffers(1, &VBO1);
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, c.coords.size()*sizeof(float),
            &c.coords[0], GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO1;
    glGenVertexArrays(1, &VAO1);
    glBindVertexArray(VAO1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                          (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);

    // copy vertex data for sphere
    GLuint VBO2;
    glGenBuffers(1, &VBO2);
    glBindBuffer(GL_ARRAY_BUFFER, VBO2);
    glBufferData(GL_ARRAY_BUFFER, s.coords.size()*sizeof(float),
                 &s.coords[0], GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO2;
    glGenVertexArrays(1, &VAO2);
    glBindVertexArray(VAO2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                          (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                          (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                          (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);

    // setup projection
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    // setup view
    Vector eye(0, 0, -2);
    Vector origin(0, 0, 0);
    Vector up(0, 1, 0);

    Matrix camera;
    camera.look_at(eye, origin, up);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // set the matrices
    Matrix model;

    mode = 0;
    glfwSetKeyCallback(window, key_callback);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(model, window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        Matrix mat = transpose(model);

        Matrix trans;
        trans.translate(cos(glfwGetTime()), 0.0, 0.0);

        model = trans;

        GLfloat lightPos [] = { 0.0, 0.0, 1.0 };
        int locationL = glGetUniformLocation(shader.id(),  "lightPos");
        glUniform3fv(locationL, 1, lightPos);

        GLfloat cameraPos [] = { 0.0, 0.0, -2.0 };
        int locationC = glGetUniformLocation(shader.id(),  "cameraPos");
        glUniform3fv(locationC, 1, cameraPos);

        Uniform::set(shader.id(), "model", model);
        Uniform::set(shader.id(), "projection", projection);
        Uniform::set(shader.id(), "camera", camera);
        Uniform::set(shader.id(), "inTrModel", mat);

        if (mode == 0) {
            // render the cube
            glBindVertexArray(VAO1);
            glDrawArrays(GL_TRIANGLES, 0, c.coords.size() * sizeof(float));
        } else {
            // render the sphere
            glBindVertexArray(VAO2);
            glDrawArrays(GL_TRIANGLES, 0, s.coords.size() * sizeof(float));
        }

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}