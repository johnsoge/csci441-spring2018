#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

using namespace std;
/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    /** YOU WILL ADD CODE STARTING HERE */
    GLuint shader = 0;

    //shader creation
    shader = glCreateShader(shaderType);
    //add source code to shader
    glShaderSource(shader, 1, &src_ptr, NULL);
    //compile new shader
    glCompileShader(shader);

    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */

    GLuint program = 0;
    program = glCreateProgram();

    //attach a type of shader to the program object, then link them
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    glLinkProgram(program);

    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

//convert window coordinates to normalized device coordinates
void w2nd (float * triangle) {

    for (int i = 0; i < 15; i+=5)
    {
        triangle[i] = (-1.0f + triangle[i] * (2.0f / 640.0f));

        triangle[i + 1] = (1.0f - triangle[i + 1] * (2.0f / 480.0f));
    }
}

int main(void) {
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }

    /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

    float triangle[15];

    //prompt for user input, x, y, red, green, blue. Then store user inputs into the specified triangle array slots
    cout << "Enter x and y coordinates: " << endl;
    cin >> triangle[0];
    cin.ignore(1, ',');
    cin >> triangle[1];
    cin.ignore(1, ':');
    cin >> triangle[2];
    cin.ignore(1, ',');
    cin >> triangle[3];
    cin.ignore(1, ',');
    cin >> triangle[4];

    cout << "Enter x and y coordinates: " << endl;
    cin >> triangle[5];
    cin.ignore(1, ',');
    cin >> triangle[6];
    cin.ignore(1, ':');
    cin >> triangle[7];
    cin.ignore(1, ',');
    cin >> triangle[8];
    cin.ignore(1, ',');
    cin >> triangle[9];

    cout << "Enter x and y coordinates: " << endl;
    cin >> triangle[10];
    cin.ignore(1, ',');
    cin >> triangle[11];
    cin.ignore(1, ':');
    cin >> triangle[12];
    cin.ignore(1, ',');
    cin >> triangle[13];
    cin.ignore(1, ',');
    cin >> triangle[14];

    //enter confirmation
    cout << "You entered: " << endl
         << triangle[0] << ", " << triangle[1] << ":" << triangle[2] << ", " << triangle[3] << ", " << triangle[4] << endl
         << triangle[5] << ", " << triangle[6] << ":" << triangle[7] << ", " << triangle[8] << ", " << triangle[9] << endl
         << triangle[10] << ", " << triangle[11] << ":" << triangle[12] << ", " << triangle[13] << ", " << triangle[14] << endl;

    //convert coordinates to normalized device coordinates
    w2nd (triangle);

    //confirmation of normalized
    cout << "Normalized: " << endl
         << triangle[0] << ", " << triangle[1] << endl
         << triangle[5] << ", " << triangle[6] << endl
         << triangle[10] << ", " << triangle[11] << endl;

    GLuint VBO[1], VAO[1];

    //generate buffer object
    glGenBuffers(1, VBO);
    //generate vertex array object
    glGenVertexArrays(1, VAO);

    //bind the vertex array object, making any more functions act within this specifc VAO
    glBindVertexArray(VAO[0]);
    //bind the VBO, making it the one to use for functions
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    //allocate and send data to buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    //create triangle
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //color triangle
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

    // create the shader program
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

    // cleanup the vertex and fragment shaders using glDeleteShader

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);

        // set the shader program using glUseProgram

        glUseProgram(shaderProgram);

        // bind the vertex array using glBindVertexArray

        glBindVertexArray(VAO[0]);

        // draw the triangles using glDrawArrays

        glDrawArrays(GL_TRIANGLES, 0, 3);

        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
