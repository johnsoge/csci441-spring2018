#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <list>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

//add ability to compare Vectors
bool operator==(const Vector &a, const Vector &b)
{
    return a.x() == b.x() && a.y() == b.y() && a.z() == b.z();
}

//add ability to add Vectors
Vector operator+(const Vector& u, const Vector& v) {
    return Vector(
            u.x() + v.x(),
            u.y() + v.y(),
            u.z() + v.z(),
            u.w() + v.w());
}

struct Face {
    Vector a = Vector(0,0,0);
    Vector b = Vector(0,0,0);
    Vector c = Vector(0,0,0);
    Vector aC = Vector(0,0,0);
    Vector bC = Vector(0,0,0);
    Vector cC = Vector(0,0,0);
    Vector aN = Vector(0,0,0);
    Vector bN = Vector(0,0,0);
    Vector cN = Vector(0,0,0);
    Vector normal = Vector(0,0,0);
};

void calculateFaceNormal(Face* face){
    Vector lineAB = face->a - face->b;
    Vector lineAC = face->c - face->b;

    Vector normal = lineAB.cross(lineAC);
    normal = normal.normalized();

    face->normal = normal;
    //std::cout << face->normal << std::endl;
}

template<typename S>
std::vector<float> findNormals(S& shape) {
    std::list <Face> triangles = {};
    Vector a(0,0,0);

    //Iterate over coords of shape
    for(std::vector<float>::iterator it = shape.coords.begin(); it != shape.coords.end();) {
        Face face;
        //record first vector for a face
        float x = *it++, y = *it++, z = *it++;
        face.a = Vector(x, y, z);
        float xN = *it++, yN = *it++, zN = *it++;
        face.aC = Vector(xN, yN, zN);
        //skip color and preset normal data
        for(int i = 6; i < 9; i++) {
            it++;
        }
        //record second vector for a face
        x = *it++, y = *it++, z = *it++;
        face.b = Vector(x, y, z);
        xN = *it++, yN = *it++, zN = *it++;
        face.bC = Vector(xN, yN, zN);
        //skip color and preset normal data
        for(int i = 6; i < 9; i++) {
            it++;
        }
        //record third vector for a face
        x = *it++, y = *it++, z = *it++;
        face.c = Vector(x, y, z);
        xN = *it++, yN = *it++, zN = *it++;
        face.cC = Vector(xN, yN, zN);
        //skip color and preset normal data
        for(int i = 6; i < 9; i++) {
            it++;
        }
        //calculate normale for the face;
        calculateFaceNormal(&face);
        triangles.push_back(face);
    }
    std::cout << "Finished calculating face normals" << std::endl;

    std::vector<float> coords;
    //iterate through all faces, calculating the average normal
    for(std::list<Face>::iterator it = triangles.begin(); it != triangles.end(); it++) {

        //Compute the average normal for the each vertice of the triangle
        Vector avgA(0,0,0);
        int countA = 0;
        Vector avgB(0,0,0);
        int countB = 0;
        Vector avgC(0,0,0);
        int countC = 0;
        for(std::list<Face>::iterator it2 = triangles.begin(); it2 != triangles.end(); it2++) {
            if(it == it2) {
                if (it->a == it2->a) {
                    avgA = avgA + it2->normal;
                    countA++;

                    std::cout << avgA << std::endl;
                }
                if (it->b == it2->b) {
                    avgB = avgB + it2->normal;
                    countB++;

                    std::cout << avgB << std::endl;
                }
                if (it->c == it2->c) {
                    avgC = avgC + it2->normal;
                    countC++;

                    std::cout << avgC << std::endl;
                }
            }

            std::cout << "Checked a face" << std::endl;
        }
        avgA.scale(1/countA);
        //Add A vertice to coords
        coords.push_back(it->a.x());
        coords.push_back(it->a.y());
        coords.push_back(it->a.z());
        //std::cout << it->a << std::endl;
        coords.push_back(it->aC.x());
        coords.push_back(it->aC.y());
        coords.push_back(it->aC.z());
        //std::cout << it->aC << std::endl;
        coords.push_back(avgA.x());
        coords.push_back(avgA.y());
        coords.push_back(avgA.z());

        avgB.scale(1/countB);
        //Add B vertice
        coords.push_back(it->b.x());
        coords.push_back(it->b.y());
        coords.push_back(it->b.z());
        coords.push_back(it->bC.x());
        coords.push_back(it->bC.y());
        coords.push_back(it->bC.z());
        coords.push_back(avgB.x());
        coords.push_back(avgB.y());
        coords.push_back(avgB.z());

        avgC.scale(1/countC);
        //Add C vertice
        coords.push_back(it->c.x());
        coords.push_back(it->c.y());
        coords.push_back(it->c.z());
        coords.push_back(it->cC.x());
        coords.push_back(it->cC.y());
        coords.push_back(it->cC.z());
        coords.push_back(avgC.x());
        coords.push_back(avgC.y());
        coords.push_back(avgC.z());
    }

    return coords;
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    Torus c(40, .75, .5, 1, .2, .4);

    // create obj
    Model obj(
            findNormals(c),
            Shader("../vert.glsl", "../frag.glsl"));

    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix floor_trans, floor_scale;
    floor_trans.translate(0, -2, 0);
    floor_scale.scale(100, 1, 100);
    floor.model = floor_trans*floor_scale;

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, 3);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(obj.model, window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object and the floor
        renderer.render(camera, obj, lightPos);
        renderer.render(camera, floor, lightPos);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
